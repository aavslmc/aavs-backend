import argparse
import logging as log
import logging.config as log_config
import os

from socketIO_client import SocketIO, SocketIONamespace as BaseNamespace

config_filepath = os.path.join(os.path.dirname(__file__), '../../configuration.ini')
log_config.fileConfig(config_filepath, disable_existing_loggers=False)


class SocketIOClient(SocketIO):
    """
    Fix for library bug
    """

    def _should_stop_waiting(self, for_connect=False, for_callbacks=False):
        if for_connect:
            for namespace in self._namespace_by_path.values():
                is_namespace_connected = getattr(
                    namespace, '_connected', False)
                # Added the check and namespace.path because for the root namespaces, which is an empty string
                # the attribute _connected is never set so this was hanging when trying to connect to namespaces
                # this skips the check for root namespace, which is implicitly connected
                if not is_namespace_connected and namespace.path:
                    return False
            return True
        if for_callbacks and not self._has_ack_callback:
            return True
        return super(SocketIO, self)._should_stop_waiting()


class EventsNamespace(BaseNamespace):
    @staticmethod
    def on_event(data):
        log.debug("EVENTS: {}".format(data["data"]))


class AlarmsNamespace(BaseNamespace):
    @staticmethod
    def on_alarm(data):
        log.debug("ALARMS: {}".format(data["data"]))


class MetricsNamespace(BaseNamespace):
    @staticmethod
    def on_metric(data):
        log.debug("METRICS: {} ".format(data["data"]))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Test Websocket Client')
    parser.add_argument('--events', action="store_true", default=False, help="Subscribe to events")
    parser.add_argument('--alarms', action="store_true", default=False, help="Subscribe to alarms")
    parser.add_argument('--metrics', action="store_true", default=False, help="Subscribe to metrics")
    args = parser.parse_args()

    with SocketIOClient('127.0.0.1', 8123) as socketIO:
        subscribe_all = not args.events and not args.alarms and not args.metrics

        if args.events or subscribe_all:
            events_namespace = socketIO.define(EventsNamespace, '/events')
            log.info("Subscribed to events")

        if args.alarms or subscribe_all:
            alarms_namespace = socketIO.define(AlarmsNamespace, '/alarms')
            log.info("Subscribed to alarms")

        if args.metrics or subscribe_all:
            alarms_namespace = socketIO.define(MetricsNamespace, '/metrics')
            log.info("Subscribed to metrics")

        try:
            socketIO.wait()
        except KeyboardInterrupt:
            socketIO.disconnect()
            log.info('Keyboard interrupt detected. Quiting.')
