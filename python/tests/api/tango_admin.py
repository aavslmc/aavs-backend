import copy
import json
import uuid
from StringIO import StringIO
from unittest import TestCase
from urllib import urlencode

from tests.api import app as api_app
from api.config import HARDWARE_COMPONENT_TYPES_LOWER, HARDWARE_COMPONENT_TYPES_DICT
from api.config.version import API_PREFIX


class TestResponse(TestCase):
    def setUp(self):
        self.app = api_app.test_client()

    def test_antenna_upload(self):
        uri = "/admin/antenna_locations"
        request_uri = API_PREFIX + uri

        antennas ={"telescope_model": {
            "antennas": [
                {
                    "altitude": 0,
                    "id": "1",
                    "latitude": 11,
                    "longitude": 12,
                    "tile_serial": "1"
                },
                {
                    "altitude": 0,
                    "id": "34",
                    "latitude": 31,
                    "longitude": 12,
                    "tile_serial": "34"
                }
            ]
        }}

        response = self.app.post(request_uri,
                                 content_type='application/json',
                                 data=json.dumps(antennas))

        print response
        self.assertEqual(response.status,'200 OK')

        data =   json.loads(self.app.get(request_uri).data)

        self.assertEqual(data, antennas)

    def test_skymodel_upload(self):
        uri = "/admin/skymodel"
        request_uri = API_PREFIX + uri

        fields =  [

            { u"name" : u"ra",
              u"description": u"Right Ascension",
              u"units": u"deg"
            } for x in xrange(8)
        ]


        skymodel =  "23.3900 58.8150 12800 0 0 0 152.0e6 0.770\n" \
                    "23.3900 58.8150 12800 0 0 0 152.0e6 0.770\n" \
                    "23.3900 58.8150 12800 0 0 0 152.0e6 0.770\n"

        print skymodel
        # response = self.app.post(request_uri, buffered=True,
        #                content_type='multipart/form-data',
        #                data={'fields': json.dumps(fields),
        #                      'data': (StringIO(skymodel), 'hello.txt')})

        response = self.app.post(request_uri,
                                 content_type='application/json',
                                 data=json.dumps({"skymodel":
                                     {
                                     'metadata':
                                            {'fields' : fields},
                                     'data': skymodel}
                                 }))
        print response
        self.assertEqual(response.status,'200 OK')

        data =   json.loads(self.app.get(request_uri).data)
        internal = {
            u"skymodel":

            {
                u"data": [map(float,l.split(" ")) for l in skymodel.splitlines()],
                u"metadata": {u"fields": fields}
            }
        }
        self.assertEqual(internal, data)