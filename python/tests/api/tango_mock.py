import time

from PyTango._PyTango import AttrWriteType
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command, device_property

from servers.abstract.AAVS_DS import AAVS_DS
from servers.abstract.Group_DS import Group_DS


class TangoTestDevice(Device):
    __metaclass__ = DeviceMeta


    def init_device(self):
        Device.init_device(self)
        self.value = "writeable"

    event_persistor_module_name = device_property(dtype=str,default_value="aavslmc.library.event_database")
    test_attribute_rw = attribute(access=AttrWriteType.READ_WRITE)
    test_attribute_ro = attribute(dtype=str, access=AttrWriteType.READ)

    def read_test_attribute_ro(self):
        return "I'm read only"

    def read_test_attribute_rw(self):
        return  self.value

    def write_test_attribute_rw(self, attr):
        self.value  = attr

    @command()
    def test_command(self):
        pass

    @command(dtype_in=str)
    def test_command(self):
        pass


def create_ds():
    import PyTango

    dev_info = PyTango.DbDevInfo()
    dev_info.server = "tango_mock/unittest"
    dev_info._class = "TangoTestDevice"
    dev_info.name = "unittest/server/1"

    db = PyTango.Database()
    db.add_device(dev_info)


if __name__ == "__main__":
    create_ds()
    run((TangoTestDevice,))
