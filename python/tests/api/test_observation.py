import json
import time
from unittest import TestCase

from api import app as api_app
from api.config.version import API_PREFIX

# A model schema to be used by the unit tests for the creation of an observation
OBSERVATION_DATA = {"observation": {"meta": {}, "jobs": {
    "pointing": {
        "channels": {
            "start": 123,
            "count": 40
        },
        "atten": 1.0,
        "altaz": {
            "alt": 84.0,
            "az": 270.0
        }
    },
    "calibration": {}
},
                                    "mode": "mwa",
                                    "stations": [
                                        {"tiles": [1, 2, 4], "id": 1}
                                    ]
                                    }
                    }


class TestResponse(TestCase):

    @staticmethod
    def _url_for(uri):
        return API_PREFIX + uri

    @staticmethod
    def _to_dict(data):
        return json.loads(data)

    def _init_obs(self):
        # Initialise a new observation
        response = self.app.post(self._url_for('/observations'), data=json.dumps(OBSERVATION_DATA),
                                 content_type='application/json')

        # Test that an observation id is returned
        assert 'observation_id' in self._to_dict(response.data)

        # Give some time for the observation to initialise
        time.sleep(3)

        # Check the status of the observation
        response = self.app.get(self._url_for('/observations/current/status'))

        # Test that the observation is initialising
        self.assertDictEqual(self._to_dict(response.data), {"status": {"value": "ON"}})

    def _start_obs(self):
        response = self.app.patch(self._url_for('/observations/current'),
                                  data=json.dumps({"operation": "start"}),
                                  content_type='application/json')

        # Check that the patch operation succeeded (no content)
        assert response.status_code == 202

        time.sleep(1)

        # Check the status of the observation
        response = self.app.get(self._url_for('/observations/current/status'))

        # Test that the observation is running
        self.assertDictEqual(self._to_dict(response.data), {"status": {"value": "RUNNING"}})

    def setUp(self):
        """
        The setup function to be run on each unit test. This ensures that a there is no
        running observation when a unit test is started.

        :return:
        """
        self.app = api_app.test_client()

        # Ensure that there is no current observation running (otherwise init will fail)
        response = self.app.get(self._url_for('/observations/current/status'), data={}, content_type='application/json')

        status = self._to_dict(response.data)['status']['value']
        if status == "ON" or status == "INIT" or status == "RUNNING":
            self.app.delete(self._url_for('/observations/current'), data={}, content_type='application/json')

    def test_init_status(self):
        """
        Test the initialisation status of an observation. When an observation is created, the INIT observation
        status should be shown.

        :return:
        """
        # Initialise a new observation
        response = self.app.post(self._url_for('/observations'), data=json.dumps(OBSERVATION_DATA),
                                 content_type='application/json')
        print response.data
        # Test that an observation id is returned
        assert 'observation_id' in self._to_dict(response.data)

        # Give some time for the observation to initialise
        time.sleep(1)

        # Check the status of the observation
        response = self.app.get(self._url_for('/observations/current/status'))

        # Test that the observation is initialising
        self.assertDictEqual(self._to_dict(response.data), {"status": {"value": "INIT"}})

    def test_on_status(self):
        """
        Test the ON status of the observation. Once the observation initialise itself, the status of the observation
        should change to ON

        :return:
        """
        # Initialise a new observation
        self._init_obs()

    def test_off_status(self):
        """
        Tests the OFF status of an observation. No observations should be running at the start of each unit test.

        :return:
        """
        # Check the status of the observation
        response = self.app.get(self._url_for('/observations/current/status'))

        # Test that the observation is initialising
        self.assertDictEqual(self._to_dict(response.data), {"status": {"value": "OFF"}})

    def test_init_status_twice(self):
        """
        Ensure an observation cannot be initialised twice.

        :return:
        """
        self.app.post(self._url_for('/observations'), data=json.dumps(OBSERVATION_DATA),
                      content_type='application/json')

        time.sleep(1)

        response = self.app.post(self._url_for('/observations'), data=json.dumps(OBSERVATION_DATA),
                                 content_type='application/json')
        data = json.loads(response.data)

        # An error should be reported by the API
        assert "error" in data

        # The error code for CONFLICT should be shown
        assert response.status_code == 409

    def test_observation_start(self):
        """
        Test the RUNNING status of the observation. Once the observation initialise itself, the status of the
        observation should change to ON. Once the status changes to ON, the observation can be started.
        The status of the started observation should be RUNNING

        :return:
        """
        # Initialise the observation
        self._init_obs()

        # Start the observation
        self._start_obs()

    def test_observation_stop(self):
        """
        Test that a RUNNING observation can be stopped successfully

        :return:
        """
        # Initialise the observation
        self._init_obs()

        # Start the observation
        self._start_obs()

        # Stop the observation
        response = self.app.patch(self._url_for('/observations/current'),
                                  data=json.dumps({"operation": "stop"}),
                                  content_type='application/json')

        # Check that the patch operation succeeded (no content)
        assert response.status_code == 202

        # Check the status of the observation
        response = self.app.get(self._url_for('/observations/current/status'))

        # Test that the observation is running
        self.assertDictEqual(self._to_dict(response.data), {"status": {"value": "OFF"}})
