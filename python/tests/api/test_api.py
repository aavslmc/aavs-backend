import copy
import json
from unittest import TestCase

from api import app as api_app
from api.config import HARDWARE_COMPONENT_TYPES_DICT
from api.config.version import API_PREFIX


class TestResponse(TestCase):
    def _test_response(self, request_uri, response, components='component', single=False):
        """
        All responses relating to components should contain component_id and component_type
        :param response:
        :return:
        """
        data = json.loads(response.data)

        # Ensure that response has a number of components
        assert components in data

        if single:
            component = data[components]
            self.assertIsNotNone(component['component_id'], 'Component ID was not specified in {}'.format(request_uri))

            # Assert component has a type associated with it
            self.assertIsNotNone(component['component_type'],
                                 'Component Type was not specified in {}'.format(request_uri))
            return

        # Iterate over all the available components
        for component in data[components]:
            # Assert components has an id
            self.assertIsNotNone(component['component_id'], 'Component ID was not specified in {}'.format(request_uri))

            # Assert component has a type associated with it
            self.assertIsNotNone(component['component_type'],
                                 'Component Type was not specified in {}'.format(request_uri))


class TestCommonComponentUris(TestResponse):

    def setUp(self):
        self.app = api_app.test_client()

    uris_component_200 = [
        ('/{component_type}', 'components', False),
        ('/{component_type}/{component_id}', 'component', True),
        ('/{component_type}/{component_id}/properties/{property_name}', 'property', True)
    ]

    # don't allow urls beyond redirect path
    uris_404 = [
        '/{component_type}/{component_id}/events/{event_id}',
        '/{component_type}/{component_id}/alarms/{alarm_id}'
        '/{component_type}/{component_id}/commands/{command_name}/command_runs/{command_run_id}',
        '/{component_type}/{component_id}/event_subscriptions/{event_subscription_id}',
    ]

    # command runs not implemented, requires tango backend
    uris_post = [
        ('/command_runs', 'command_run'),
    ]

    uris_patch_list = [
        ('/{component_type}/{component_id}/properties', 'properties'),
        ('/{component_type}/properties/{property_name}', 'properties'),
    ]

    uris_patch = [
        ('/{component_type}/{component_id}/properties/{property_name}', 'property'),
    ]

    # we are running with a dummy server to test the api functionality, so one is enough
    component_list = ['servers']

    format_dict = {
        'source_id': 1,
        'component_id': 1,
        'source_type': 'tile',
        'command_name': 'to_json',
        'property_name': 'test_attribute_rw',
        'name': 'test_attribute_rw',
        'alarm_id': '34b8335b-4895-4d69-8288-70742a6e554f',
        'event_id': 'd141db65-217d-45d0-afb7-0b7ba7021aac',
        'command_run_id': '538a6b80-a321-4e3d-a1dd-f0e8f291e0ab',
        'event_subscription_id': '9b951326-9c0f-4741-8247-bec8e831d7ce',
    }

    def test_patch_returns_404(self):
        format_dict = self.format_dict.copy()
        format_dict['command_name'] = 'commanddoesnotexist'
        format_dict['property_name'] = 'propertydoesnotexist'
        format_dict['name'] = 'propertydoesnotexist'
        format_dict['value'] = 'blah'
        for component in self.component_list:
            format_dict['component_type'] = component

            for uri, desc in self.uris_patch:
                self._patch(desc, format_dict, uri, code=404)
                format_dict['component_id'] = 1000
                self._patch(desc, format_dict, uri, code=404)

            for uri, desc in self.uris_patch_list:
                format_dict['component_id'] = 1000
                self._patch(desc, format_dict, uri, as_list=True, code=404)

    def _patch(self, desc, format_dict, uri, as_list=False, code=200):
        data = {desc: format_dict}
        if as_list:
            data = {desc: [format_dict]}
        request_uri = API_PREFIX + uri.format(**format_dict)
        response = self.app.patch(
            request_uri,
            data=json.dumps(data),
            content_type='application/json')

        self.assertEqual(
            response.status_code,
            code,
            'Failed: %s. %s' % (request_uri, response.data))

    def test_component_gets_return_200(self):
        for component in self.component_list:
            format_dict = copy.deepcopy(self.format_dict)
            format_dict['command_name'] = format_dict['command_name']
            format_dict['property_name'] = 'state'
            format_dict['component_type'] = component

            for uri, component_type, single in self.uris_component_200:
                request_uri = API_PREFIX + uri.format(**format_dict)
                response = self.app.get(request_uri)
                self.assertEqual(response.status_code, 200,
                                 'Failed: %s. %s %s' % (request_uri, response.status_code, response.data))
                self._test_response(request_uri, response, components=component_type, single=single)

    def test_common_404(self):
        for component in self.component_list:
            format_dict = copy.deepcopy(self.format_dict)
            format_dict['command_name'] = HARDWARE_COMPONENT_TYPES_DICT.get(component) + self.format_dict[
                'command_name']
            format_dict['component_type'] = component
            for uri in self.uris_404:
                request_uri = API_PREFIX + uri.format(**format_dict)
                response = self.app.delete(
                    request_uri)
                self.assertEqual(response.status_code, 404, 'Failed: %s. %s' % (request_uri, response.data))
