import PyTango
from pydaq.persisters import *
from aavslmc.library.utils import JobTasks, DataLevel, decode_task_type, task_type_string


class DaqInterface(object):

    def __init__(self, tango_domain="test"):
        self.tango_domain = tango_domain
        self.obsconf_dp = PyTango.DeviceProxy('%s/obsconf/main' % tango_domain)
        self.obsconf_dp.set_timeout_millis(50000)

        self.n_channels = self.obsconf_dp["daq_nof_channels"].value
        self.n_antennas = self.obsconf_dp["daq_nof_antennas"].value
        self.n_pols = self.obsconf_dp["daq_nof_pols"].value
        self.n_raw_samples = self.obsconf_dp["daq_raw_samples"].value
        self.n_channel_samples = self.obsconf_dp["daq_channel_samples"].value
        self.n_beam_samples = self.obsconf_dp["daq_beam_samples"].value
        self.obs_data_directory = self.obsconf["daq_data_directory"].value

        # not yet integrated
        self.n_correlator_stokes = 0
        self.n_correlator_samples = 1
        self.n_correlator_baselines = 1
        # self.n_correlator_stokes = self.obsconf_dp["daq_correlator_stokes"].value
        # self.n_correlator_samples = self.obsconf_dp["daq_correlator_samples"].value
        # self.n_correlator_baselines = self.obsconf_dp["daq_correlator_baselines"].value

    def get_data_by_file(self, data_type=JobTasks.Burst_Raw, data_level=DataLevel.Tile, station_id=1, tile_id=0):
        station_data_directory = os.path.join(self.obs_data_directory,str(station_id))

        if data_level is DataLevel.Tile:
            if data_type is JobTasks.Burst_Raw:
                fm = RawFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Void)
                fm.set_metadata(n_antennas=self.n_antennas, n_pols=self.n_pols,
                                n_samples=self.n_raw_samples, n_chans=self.n_channels)
                data, timestamps = fm.read_data(timestamp=None, tile_id=tile_id, antennas=range(0, self.n_antennas),
                                    polarizations=range(0, self.n_pols), n_samples=self.n_raw_samples)
            if data_type is JobTasks.Burst_Channel:
                fm = ChannelFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Void)
                fm.set_metadata(n_antennas=self.n_antennas, n_pols=self.n_pols,
                                n_samples=self.n_channel_samples, n_chans=self.n_channels)
                data, timestamps = fm.read_data(timestamp=None, tile_id=tile_id, antennas=range(0, self.n_antennas),
                                    channels=range(0, self.n_channels), polarizations=range(0, self.n_pols),
                                    n_samples=self.n_channel_samples)
            if data_type is JobTasks.Burst_Beam:
                fm = BeamFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Void)
                fm.set_metadata(n_chans=self.n_channels, n_pols=self.n_pols, n_samples=self.n_beam_samples)
                data, timestamps = fm.read_data(timestamp=None, tile_id=tile_id, channels=range(0, self.n_channels),
                                    polarizations=range(0, self.n_pols), n_samples=self.n_beam_samples)
            if data_type is JobTasks.Integrated_Channel:
                fm = ChannelFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Integrated)
                fm.set_metadata(n_antennas=self.n_antennas, n_pols=self.n_pols,
                                n_samples=self.n_channel_samples, n_chans=self.n_channels)
                data, timestamps = fm.read_data(timestamp=None, tile_id=tile_id, antennas=range(0, self.n_antennas),
                                    channels=range(0, self.n_channels), polarizations=range(0, self.n_pols),
                                    n_samples=self.n_channel_samples)
            if data_type is JobTasks.Integrated_Beam:
                fm = BeamFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Integrated)
                fm.set_metadata(n_chans=self.n_channels, n_pols=self.n_pols, n_samples=self.n_beam_samples)
                data, timestamps = fm.read_data(timestamp=None, tile_id=tile_id, channels=range(0, self.n_channels),
                                                polarizations=range(0, self.n_pols), n_samples=self.n_beam_samples)

        if data_level is DataLevel.Station:
            last_tile_id = 16

            if data_level is JobTasks.Burst_Station:
                fm = ChannelFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Void)
                fm.set_metadata(n_antennas=self.n_antennas, n_pols=self.n_pols,
                        n_samples=self.n_channel_samples, n_chans=self.n_channels)
                data, timestamps = fm.read_data(timestamp=None, tile_id=last_tile_id, antennas=range(0, self.n_antennas),
                                                channels=range(0, self.n_channels), polarizations=range(0, self.n_pols),
                                                n_samples=self.n_channel_samples)
            if data_level is JobTasks.Integrated_Station_Beam:
                fm = BeamFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Integrated)
                fm.set_metadata(n_chans=self.n_channels, n_pols=self.n_pols, n_samples=self.n_beam_samples)
                data, timestamps = fm.read_data(timestamp=None, tile_id=last_tile_id, channels=range(0, self.n_channels),
                                                polarizations=range(0, self.n_pols), n_samples=self.n_beam_samples)
            if data_level is JobTasks.Correlator:
                fm = CorrelationFormatFileManager(root_path=station_data_directory, daq_mode=FileDAQModes.Void)
                fm.set_metadata(n_antennas=self.n_antennas,
                                         n_pols=self.n_pols,
                                         n_chans=self.n_channels,
                                         n_stokes=self.n_correlator_stokes,
                                         n_samples=self.n_correlator_samples,
                                         n_baselines=self.n_correlator_baselines)
                data, timestamps = fm.read_data(timestamp=None, tile_id=last_tile_id, channels=range(0, self.n_channels),
                                               antennas=range(0, self.n_antennas), polarizations=range(0, self.n_pols),
                                               n_samples=self.n_correlator_samples)

        return data, timestamps

def main():
    try:
        daqsystem = DaqInterface()
        data, timestamps = daqsystem.get_data_by_file(data_type=JobTasks.Burst_Raw,
                                                      data_level=DataLevel.Tile,
                                                      station_id=1,
                                                      tile_id=0)
        print "Done"
    except Exception,e:
        print 'Exception: ',e

if __name__ == '__main__':
    main()