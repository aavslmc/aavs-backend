#!/usr/bin/env bash


curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 34,
		\"command_name\": \"connect\",
		\"parameters\": [{
			\"name\": \"initialise\",
			\"value\": \"True\"
		}, {
			\"name\": \"simulation\",
			\"value\": \"False\"
		}]
	}
}" "http://localhost/aavs/v1/command_runs"


curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 34,
		\"command_name\": \"initialise_tpm\"
	}
}" "http://localhost/aavs/v1/command_runs"
