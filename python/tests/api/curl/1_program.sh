#!/usr/bin/env bash

curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 34,
		\"command_name\": \"connect\",
		\"parameters\": [{
			\"name\": \"initialise\",
			\"value\": \"False\"
		}, {
			\"name\": \"simulation\",
			\"value\": \"True\"
		}]
	}
}" "http://localhost/aavs/v1/command_runs"

curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 34,
		\"command_name\": \"program_fpgas\",
		\"parameters\": [{
			\"name\": \"bitfile\",
			\"value\": \"/opt/aavs/bitfiles/itpm_v1_1_tpm_test_wrap_integrator28.bit\"
		}]
	}
}" "http://localhost/aavs/v1/command_runs"

curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 34,
		\"command_name\": \"disconnect\"
	}
}" "http://localhost/aavs/v1/command_runs"
