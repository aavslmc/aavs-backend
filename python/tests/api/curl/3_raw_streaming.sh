#!/usr/bin/env bash


curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 34,
		\"command_name\": \"stop_data_transmission\",
		\"parameters\": [{
			\"name\": \"mode\",
			\"value\": \"ALL\"
		}]
	}
}" "http://localhost/aavs/v1/command_runs"

curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 34,
		\"command_name\": \"send_raw_data\",
		\"parameters\": []
}
}" "http://localhost/aavs/v1/command_runs"
