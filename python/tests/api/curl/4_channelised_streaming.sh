#!/usr/bin/env bash

curl -i -X PATCH --header "Content-Type: application/json" --header "Accept: application/json" -d "{\"property\": {
\"value\": \"128\",
\"type\": \"int\"}}" "http://localhost:5000/aavs/v1/tiles/1/properties/channelised_spectra"

curl -i -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
	\"command_run\": {
		\"component_type\": \"tile\",
		\"component_id\": 1,
		\"command_name\": \"start_poll_command\",
		\"parameters\": [{
			\"name\": \"poll_command\",
			\"value\": \"send_channelised_data\"
		},{
			\"name\": \"period\",
			\"value\": \"5000\"
		}]
	}
}" "http://localhost:5000/aavs/v1/command_runs"