import json
from unittest import TestCase

import time

from api import app
from api.config.version import API_PREFIX


class TestResponse(TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_configured(self):
        uri = "/alarms/configured"
        request_uri = API_PREFIX + uri
        response = self.app.get(request_uri)
        print response.data

    def test_active(self):
        uri = "/alarms/active"
        request_uri = API_PREFIX + uri
        response = self.app.get(request_uri)
        print response.data

    def test_alarm(self):

        uri = "/observations/current"
        request_uri = API_PREFIX + uri
        response = self.app.delete(request_uri)
        print response.data, response.status

        while True:
            uri = "/observations/current/status"
            request_uri = API_PREFIX + uri
            response = self.app.get(request_uri)
            status_dict = json.loads(response.data)
            status = status_dict["status"]["value"]
            time.sleep(1)
            if status == "OFF":
                break

        uri = "/alarms"
        request_uri = API_PREFIX + uri
        delete_dict = {
            "alarm_subscription": {
                "attribute": "State",
                "component_id": "obs",
                "component_type": "observation"
            }
        }
        response = self.app.delete(request_uri, data=json.dumps(delete_dict), content_type='application/json')
        print response.data, response.status

        uri = "/alarms"
        request_uri = API_PREFIX + uri
        alarm_dict = {
            "alarm_subscription": {
                "absolute": "FAULT",
                "attribute": "State",
                "component_id": "obs",
                "component_type": "observation"
            }
        }
        response = self.app.post(request_uri, data=json.dumps(alarm_dict), content_type='application/json')
        print response.data, response.status

        obs = {"observation": {"mode": "mwa", "jobs":
            {"pointing": {"radec": {"ra": "0", "dec": "0"}, "altaz": {"az": "0", "alt": "0"},
                          "channels": {"start": "0", "count": "0"}, "atten": "0"}},
                               "stations": [{"id": "1", "tiles": [1]}]}}

        uri = "/observations"
        request_uri = API_PREFIX + uri

        response = self.app.post(request_uri, data=json.dumps(obs), content_type='application/json')
        print response.data, response.status

    def test_alarm_racks(self):
        uri = "/alarms"
        request_uri = API_PREFIX + uri
        delete_dict = {
            "alarm_subscription": {
                "attribute": "members_state",
                "component_id": "1",
                "component_type": "rack"
            }
        }
        response = self.app.delete(request_uri, data=json.dumps(delete_dict), content_type='application/json')
        print response.data

        uri = "/alarms"
        request_uri = API_PREFIX + uri
        alarm_dict = {
            "alarm_subscription": {
                "absolute": "ALARM",
                "attribute": "members_state",
                "component_id": "1",
                "component_type": "rack"
            }
        }
        response = self.app.post(request_uri, data=json.dumps(alarm_dict), content_type='application/json')
        print response.data
