from api.library.common import ResourceNotFoundException

from components_tango import ComponentTangoManager as ComponentManager
from events_tango import EventSubscriptionManager as EventTangoManager
from alarms_tango import AlarmManager as AlarmTangoManager

from process_base import (
    AlarmManagerInterface as AlarmManager,
    EventSubscriptionManagerInterface as EventSubscriptionManager,
    FirmwareUploadManagerInterface as FirmwareUploadManager,
    CommandRunsManagerInterface as CommandRunsManager
)