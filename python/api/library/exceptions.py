class LibraryValidationException(Exception):
    pass


class ResourceNotFoundException(Exception):
    pass


class BadRequestException(Exception):
    pass


class ObservationExistsException(Exception):
    pass


class TangoNotFoundException(Exception):
    def __init__(self, id_=None, type_=None):
        self.id_ = id_
        self.type_ = type_

        message = 'Id %s for type %s was not found' % (
            self.id_, self.type_
        )

        super(TangoNotFoundException, self).__init__(message)
