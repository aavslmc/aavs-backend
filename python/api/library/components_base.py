from api.library.common import error_handler


class MockRepository(object):
    pass


SINGLE_MAP = {
    'station': 'station',
    'tile': 'tile',
    'pdu': 'component',
    'switch': 'component',
    'server': 'component',
    'rack': 'rack',
    'observation': 'observation'
}

MULTIPLE_MAP = {
    'station': 'stations',
    'tile': 'tiles',
    'pdu': 'components',
    'switch': 'components',
    'server': 'components',
    'rack': 'racks',
    'observation': 'observations'
}

PLURAL_MAP = {
    'station': 'stations',
    'tile': 'tiles',
    'pdu': 'pdus',
    'switch': 'switches',
    'server': 'servers',
    'rack': 'racks'
}

SKIP = 0
LIMIT = 10


class ComponentManagerInterface(object):
    def _create_firmware_upload(self):
        return {}

    def create_firmware_upload(self, **kwargs):
        data = self._create_firmware_upload(**kwargs)
        return {
            'firmware_upload': data
        }

    def _get_status(self, component_type, **kwargs):
        return {}, []

    @error_handler
    def get_status(self, component_type, **kwargs):
        data, errors = self._get_status(component_type, **kwargs)
        return self.extract_details("status", data, errors)

        # return {
        #     'status': data
        # }

    def _get(self, component_type, **kwargs):
        return {}, []

    @error_handler
    def get(self, component_type, **kwargs):
        data = self._get(component_type, **kwargs)
        return {
            SINGLE_MAP[component_type]: data
        }

    def _get_property(self, component_type, component_id, property_name, **kwargs):
        return {}, []

    @error_handler
    def get_property(self, component_type, **kwargs):
        data, errors = self._get_property(component_type, **kwargs)
        return self.extract_details("property", data, errors)
        #
        # return {
        #     'property': data
        # }

    def _get_command(self, component_type, **kwargs):
        return {}

    @error_handler
    def get_command(self, component_type, **kwargs):
        data = self._get_command(component_type, **kwargs)
        return {
            'command': data
        }

    def _get_firmware(self, component_type, name, **kwargs):
        return {}

    @error_handler
    def get_firmware(self, component_type, name, **kwargs):
        data = self._get_firmware(component_type, name, **kwargs)
        return {
            'firmware': data
        }

    def _get_firmware_upload(self, component_type, **kwargs):
        return {}

    @error_handler
    def get_firmware_upload(self, component_type, **kwargs):
        data = self._get_firmware_upload(component_type, **kwargs)
        return {
            'firmware_upload': data
        }

    def _update_one(self, component_type, **kwargs):
        return {}

    @error_handler
    def update_one(self, component_type, **kwargs):
        data = self._update_one(component_type, **kwargs)
        return {
            SINGLE_MAP[component_type]: data
        }

    def _update_properties(self, component_type, **kwargs):
        return {}

    @error_handler
    def update_properties(self, component_type, **kwargs):
        properties = kwargs.get("properties")
        property = kwargs.get("property")
        property_name = kwargs.get("property_name")

        property_dict_list = properties or ([property] if property else [])
        # TODO this might not be needed because of alarm server
        self._validate_name_match(property_dict_list, property_name)
        # TODO fail with 400 bad request if value is not set
        self._validate_component_type_match(property_dict_list, component_type)

        return self._update_properties(component_type, property_dict_list=property_dict_list, **kwargs)

    def _update_command(self, component_type, **kwargs):
        return {}

    @error_handler
    def update_command(self, component_type, **kwargs):
        data = self._update_command(component_type, **kwargs)
        return {
            'command': data
        }

    def _filter(self, component_type, **kwargs):
        return [], []

    @error_handler
    def filter(self, component_type, **kwargs):
        data, errors = self._filter(component_type, **kwargs)
        return self.extract_details(MULTIPLE_MAP[component_type], data, errors)

    def extract_details(self, name, data, errors):

        if errors and type(data) is list and len(data) == len(errors):
            status = "ERROR"
        elif len(errors):
            status = "WARNING"
        else:
            status = "OK"
        # TODO this breaks when we are getting the status field! shit
        return {
            name: data,
            "request_errors": errors,
            "request_status": status
        }

    # TODO not in use  remove
    @error_handler
    def filter_tiles(self, component_type, **kwargs):
        data = self._filter_tiles(component_type, **kwargs)
        return {
            MULTIPLE_MAP[component_type]: list(data)
        }

    def _filter_status(self, component_type, **kwargs):
        return [], []

    @error_handler
    def filter_status(self, component_type, **kwargs):
        data, errors = self._filter_status(component_type, **kwargs)
        return self.extract_details("statuses", data, errors)

        # return {
        #     'statuses': data
        # }

    def _filter_properties(self, component_type, with_value=False, **kwargs):
        return [], []

    @error_handler
    def filter_properties(self, component_type, component_ids=None, **kwargs):
        data, errors = self._filter_properties(component_type, component_ids=component_ids, **kwargs)
        return self.extract_details("properties", data, errors)

    def _filter_commands(self, component_type, **kwargs):
        return []

    @error_handler
    def filter_commands(self, component_type, component_ids=None, **kwargs):
        data = self._filter_commands(component_type, component_ids=component_ids, **kwargs)
        return {
            'commands': data
        }

    def _filter_firmware_uploads(self, component_type, **kwargs):
        return []

    @error_handler
    def filter_firmware_uploads(self, component_type, **kwargs):
        data = self._filter_firmware_uploads(component_type, **kwargs)
        return {
            'firmware_uploads': data
        }

    def _filter_firmwares(self, component_type, **kwargs):
        return []

    @error_handler
    def filter_firmwares(self, component_type, **kwargs):
        data = self._filter_firmwares(component_type, **kwargs)
        return {
            'firmwares': data
        }

    def init_observation(self, telescope_model):
        pass

    #     TODO should this be a command run?

    def start_observation(self, argin=''):
        pass

    def stop_observation(self, argin=''):
        pass

    def _validate_component_type_match(self, property_dict_list, component_type):
        """
        Assign component_type to property_dict if not present.
        If present, ensure component_type inside property dict matches url property name
        :param property_dict_list:
        :param component_type:
        :return:
        """
        if component_type:
            for property_dict in property_dict_list:
                data_component_type = property_dict.get('component_type')
                if data_component_type:
                    expected_type = PLURAL_MAP[component_type]
                    if data_component_type != expected_type:
                        raise Exception('Data does not match URL, expected "{}" but received "{}"'
                                        .format(expected_type, data_component_type))
                else:
                    property_dict['component_type'] = component_type

        else:
            for property_dict in property_dict_list:
                if not property_dict.get('component_type'):
                    raise Exception('Missing name in property dict')

    def _validate_name_match(self, property_dict_list, property_name):
        #  Ensure name inside property dict matches url property name, if any
        if property_name:
            for property_dict in property_dict_list:
                property_dict['name'] = property_dict.get('name', property_name)
                if property_dict['name'] != property_name:
                    raise Exception('Data does not match URL, expected {}, received {}'
                                    .format(property_name, property_dict['name']))
        else:
            for property_dict in property_dict_list:
                if not property_dict.get('name'):
                    raise Exception('Missing name in property dict')
