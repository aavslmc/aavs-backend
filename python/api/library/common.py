import json
import pydoc
from collections import defaultdict

from PyTango import DevFailed
from api.library.exceptions import TangoNotFoundException, ResourceNotFoundException, BadRequestException, \
    ObservationExistsException

VALID_TYPES = ['int', 'bool', 'str', 'float']


def tango_error_handler(function):
    """
    Wrapping the errors from the repository layer into the library layer
    :param function: request handler function
    :return:
    """

    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except DevFailed, ex:
            if ex.args:
                if ex.args[0].reason == "DB_DeviceNotDefined":
                    raise TangoNotFoundException()
                elif ex.args[0].reason == "API_UnsupportedAttribute":
                    raise ResourceNotFoundException(ex)
                elif ex.args[0].desc.strip() == "MissingValueError":
                    raise BadRequestException(ex)  # Refactor this to use throw in origin
                elif ex.args[0].reason == "ObservationExistsError":
                    raise ObservationExistsException(ex.args[0].desc)
                elif ex.args[0].reason == "NotFound":
                    raise ResourceNotFoundException(ex.args[0].desc)
            raise Exception(ex)

    return wrapper


def error_handler(function):
    """
    Wrapping the errors from the repository layer into the library layer
    :param function: request handler function
    :return:
    """

    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except TangoNotFoundException, ex:
            raise ResourceNotFoundException(ex)

    return wrapper


def get_common_properties(dict_list):
    return {u'properties': get_common_dicts(dict_list, ['name', 'type', 'component_type'])}


def get_common_commands(dict_list):
    return {u'commands': get_common_dicts(dict_list, ['name', 'parameters', 'component_type'])}


def get_common_dicts(dict_list, keys_to_compare=None):
    common_property_set = set()
    component_id_dict = defaultdict(list)

    if not keys_to_compare:
        return dict_list

    # organise dicts by component_type
    for property_dict in dict_list:
        component_id_dict[property_dict.get('component_id')].append(property_dict)

    # create hashable string of dict to compare across components
    for properties_list in component_id_dict.values():
        property_set = set()
        for property_dict in properties_list:
            property_json = json.dumps(dict([(key, property_dict.get(key)) for key in keys_to_compare]), sort_keys=True)
            property_set.add(property_json)

        if not common_property_set:
            common_property_set = property_set
        else:
            common_property_set = common_property_set.intersection(property_set)

    # rebuild dicts of common key, value
    common_properties_dicts = [json.loads(property_json) for property_json in common_property_set]

    return common_properties_dicts


def convert_api_value(param_dict):
    type_str = param_dict.get('type', 'str').lower()
    if type_str not in VALID_TYPES:
        raise Exception('Valid types must be from %s' % ', '.join(VALID_TYPES))

    value_type = pydoc.locate(type_str)
    if value_type == bool:
        if not param_dict.get('value').lower() in ['true', 'false']:
            raise Exception('Parameter value %s is not of type %s' % (param_dict.get('value'), value_type))
        value = param_dict.get('value').lower() == 'true'
    else:
        value = value_type(param_dict.get('value'))
    return param_dict.get('name'), value
