import json

import PyTango

from api.config.version import TANGO_DOMAIN
from api.library.common import tango_error_handler
from api.library.process_base import AlarmManagerInterface


class AlarmManager(AlarmManagerInterface):
    def __init__(self, tango_domain=TANGO_DOMAIN):
        self.tango_domain = tango_domain
        self.lmc_dp = PyTango.DeviceProxy('%s/lmc/1' % tango_domain)
        self.lmc_dp.set_timeout_millis(30000)

    @tango_error_handler
    def _add_alarm_subscription(self, absolute, min, max, component_type, component_id, attribute):
        self.lmc_dp.command_inout("add_alarm_subscription", json.dumps({

            "absolute": absolute,
            "min": min,
            "max": max,
            "component_type": component_type,
            "component_id": component_id,
            "attribute": attribute
        }))

    @tango_error_handler
    def _remove_alarm_subscription(self, component_type, component_id, attribute):
        self.lmc_dp.command_inout("remove_alarm_subscription", json.dumps({"component_type": component_type,
                                                                           "component_id": component_id,
                                                                           "attribute": attribute}))

    @tango_error_handler
    def _get_configured(self, component_type, component_id):
        data = self.lmc_dp.command_inout("get_configured_alarms", json.dumps({"component_type": component_type,
                                                                              "component_id": component_id}))
        return json.loads(data)

    @tango_error_handler
    def _get_active(self, component_type, component_id):
        data = self.lmc_dp.command_inout("get_active_alarms", json.dumps({"component_type": component_type,
                                                                          "component_id": component_id}))
        return json.loads(data)
