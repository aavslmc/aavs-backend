class CommandRunsManagerInterface(object):

    def _create(self, command_name, component_type, component_id=None):
        return {}

    def _get(self, command_name, component_type, component_id=None):
        return {}

    def _filter(self, command_name, component_type, component_id=None):
        return []

    def _delete(self, command_name, component_type, component_id=None):
        return None

    def create(self, command_name, component_type, component_id=None):
        return self._create(command_name, component_type, component_id)

    def get(self, command_name, component_type, component_id=None):
        return self._get(command_name, component_type, component_id)

    def filter(self, command_name, component_type, component_id=None):
        return self._filter(command_name, component_type, component_id)

    def delete(self, command_name, component_type, component_id=None):
        return self._delete(command_name, component_type, component_id)


class EventSubscriptionManagerInterface(object):

    def _add(self, event_type, component_type, component_id, attribute):
        return {}

    def add(self, event_type, component_type, component_id, attribute):
        data = self._add(event_type, component_type, component_id, attribute)
        return {
            'event_subscription': data
        }

    def _get(self, event_type, component_type, component_id, attribute):
        return {}

    def get(self, event_type, component_type, component_id, attribute):
        data = self._get(event_type, component_type, component_id, attribute)
        return {
            'event_subscription': data
        }

    def _filter(self,event_type, component_type, component_id, attribute):
        return []

    def filter(self, event_type, component_type, component_id, attribute):
        data = self._filter(event_type, component_type, component_id, attribute)
        return {
            'event_subscriptions': data
        }

    def _delete(self, event_type, component_type, component_id, attribute):
        return None

    def delete(self, event_type, component_type, component_id, attribute):
        data = self._delete(event_type, component_type, component_id, attribute)
        return data


class AlarmManagerInterface(object):

    def add_alarm_subscription(self, absolute, min, max, component_type, component_id, attribute):
        self._add_alarm_subscription( absolute, min, max, component_type, component_id, attribute)

    def _add_alarm_subscription(self, absolute, min, max, component_type, component_id, attribute):
        pass

    def _remove_alarm_subscription(self, component_type, component_id, attribute):
        pass

    def _get_configured(self, component_type, component_id):
        return []

    def get_configured(self, component_type, component_id):
        data = self._get_configured(component_type, component_id)
        return {"configured_alarms": data}

    def _get_active(self, component_type, component_id):
        return []

    def get_active(self, component_type, component_id):
        data  = self._get_active(component_type, component_id)
        return {"active_alarms": data}

    def remove_alarm_subscription(self, component_type, component_id, attribute):
        data = self._remove_alarm_subscription( component_type, component_id, attribute)
        return {
            'alarm_subscription': data
        }

    def _get(self, alarm_id):
        return {}

    def get(self, alarm_id):
        data = self._get(alarm_id)
        return {
            'alarm': data
        }

    def _filter(self, **kwargs):
        return []

    def filter(self, **kwargs):
        data = self._filter(**kwargs)
        return {
            'alarms': data
        }

    def _update_one(self, **kwargs):
        return {}

    def update_one(self, **kwargs):
        data = self._update_one(**kwargs)
        return {
            'alarm': data
        }


class FirmwareUploadManagerInterface(object):

    def _get(self, alarm_id):
        return {}

    def get(self, alarm_id):
        data = self._get(alarm_id)
        return {
            'firmware_upload': data
        }

    def _filter(self, **kwargs):
        return []

    def filter(self, **kwargs):
        data = self._filter(**kwargs)
        return {
            'firmware_uploads': data
        }

    def _update_one(self, **kwargs):
        return {}

    def update_one(self, **kwargs):
        data = self._update_one(**kwargs)
        return {
            'firmware_upload': data
        }
