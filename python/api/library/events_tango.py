import json

import PyTango

from api.config.version import TANGO_DOMAIN
from api.library.common import tango_error_handler
from api.library.process_base import EventSubscriptionManagerInterface


class EventSubscriptionManager(EventSubscriptionManagerInterface):
    def __init__(self, tango_domain=TANGO_DOMAIN):
        self.tango_domain = tango_domain
        self.lmc_dp = PyTango.DeviceProxy('%s/lmc/1' % tango_domain)
        self.lmc_dp.set_timeout_millis(30000)
        self.events_dp = PyTango.DeviceProxy('%s/EventStream/1' % tango_domain)
        self.events_dp.set_timeout_millis(30000)

    @tango_error_handler
    def _add(self, event_type, component_type, component_id, attribute):
        if event_type == "device":
            self.events_dp.command_inout('subscribe_to_device',
                                         json.dumps({"device_name": "{}/{}/{}".format(self.tango_domain,
                                                                                      component_type,
                                                                                      component_id)}))
        elif event_type == "attribute" and component_id:
            self.events_dp.command_inout('subscribe_to_attribute',
                                         json.dumps({"attribute_name": "{}/{}/{}/{}".format(self.tango_domain,
                                                                                            component_type,
                                                                                            component_id,
                                                                                            attribute)}))

    @tango_error_handler
    def _get(self, event_type, component_type, component_id, attribute):
        pass

    @tango_error_handler
    def _filter(self, event_type, component_type, component_id, attribute):
        result = self.events_dp.command_inout('get_subscriptions', json.dumps({
            "event_type": event_type,
            "component_id": component_id,
            "component_type": component_type,
            "attribute": attribute
        }))
        return json.loads(result)

    @tango_error_handler
    def _delete(self, event_type, component_type, component_id, attribute):
        if event_type == "device":
            self.events_dp.command_inout('unsubscribe_from_device',
                                         json.dumps({"device_name": "{}/{}/{}".format(self.tango_domain,
                                                                                      component_type,
                                                                                      component_id)}))
        elif event_type == "attribute" and component_id:
            self.events_dp.command_inout('unsubscribe_from_attribute',
                                         json.dumps({"attribute_name": "{}/{}/{}/{}".format(self.tango_domain,
                                                                                            component_type,
                                                                                            component_id,
                                                                                            attribute)}))
