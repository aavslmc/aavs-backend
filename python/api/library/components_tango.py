import base64
import copy
import json

import PyTango
from aavslmc.library.utils import JobTasks, DataLevel, decode_task_type
from pydaq.persisters import *

from api.config import get_tango_address, get_tango_device_type_id, get_command_parameters
from api.config.version import TANGO_DOMAIN
from api.library.common import tango_error_handler
from api.library.components_base import ComponentManagerInterface


def _build_cmd_dict(device_name, command):
    component_type, component_id = get_tango_device_type_id(device_name)
    cmd_dict = {
        'component_type': component_type,
        'component_id': component_id,
        'name': command.cmd_name,
        'parameters': get_command_parameters(command.in_type_desc)
    }
    return cmd_dict


def _get_all_commands_on_tango_group(device_group, with_value=False, **kwargs):
    cmd_list = []
    for device_name in device_group.get_device_list():
        device_proxy = PyTango.DeviceProxy(device_name)
        device_proxy.set_timeout_millis(10000)
        for command in device_proxy.command_list_query():
            cmd_dict = _build_cmd_dict(device_name, command)
            cmd_list.append(cmd_dict)
    return cmd_list


class ComponentTangoManager(ComponentManagerInterface):
    def __init__(self, tango_domain=TANGO_DOMAIN):
        self.tango_domain = tango_domain
        self.lmc_dp = PyTango.DeviceProxy('%s/lmc/1' % tango_domain)
        self.lmc_dp.set_timeout_millis(50000)
        self.events_dp = PyTango.DeviceProxy('%s/EventStream/1' % tango_domain)

    def _create_firmware_upload(self):
        return {}

    def _lmc_dp_command_run(self, command_name, component_type, component_id=None, **kwargs):
        command_args = {
            'device_type': component_type,
        }
        if component_id:
            command_args['device_id'] = component_id
        command_args.update(kwargs)
        result = self.lmc_dp.command_inout(command_name, json.dumps(command_args))
        return json.loads(result)

    @tango_error_handler
    def _get(self, component_type, component_id, **kwargs):
        result = self._lmc_dp_command_run(
            'get', component_type, component_id, **kwargs)
        return result

    @tango_error_handler
    def _get_status(self, component_type, component_id, **kwargs):
        command_args = copy.deepcopy(kwargs)
        command_args['attribute_name'] = 'visual_state'
        command_args['with_value'] = 'True'
        result = self._lmc_dp_command_run(
            'get_attributes', component_type, component_id, **command_args)

        return result["content"][0], result["errors"]

    @tango_error_handler
    def _get_property(self, component_type, component_id, property_name, with_value=False, **kwargs):
        result = self._lmc_dp_command_run(
            'get_attributes', component_type, component_id, attribute_name=property_name, with_value=with_value, **kwargs)
        return result["content"][0], result["errors"]

    @tango_error_handler
    def _get_command(self, component_type, component_id, command_name, **kwargs):
        device_name = get_tango_address(component_type, component_id)
        device_proxy = PyTango.DeviceProxy(device_name)
        device_proxy.set_timeout_millis(10000)
        device_command = device_proxy.command_query(command_name)
        command_dict = {
            'component_id': component_id,
            'component_type': component_type,
            'command_name': device_command.cmd_name,
            'parameters': get_command_parameters(device_command.in_type_desc)
        }
        return command_dict

    def _filter(self, component_type, **kwargs):
        result = self._lmc_dp_command_run(
            'filter', component_type, **kwargs)
        return result["content"], result["errors"]

    def _filter_status(self, component_type, **kwargs):
        result = self._lmc_dp_command_run(
            'get_attributes', component_type, attribute_name='visual_state', **kwargs)
        return result["content"], result["errors"]

    def _filter_properties(self, component_type, component_id=None, with_value=False, **kwargs):
        result = self._lmc_dp_command_run(
            'get_attributes', component_type, component_id, with_value=with_value, **kwargs)
        return result["content"], result["errors"]

    @tango_error_handler
    def _update_properties(self, component_type, component_ids=None, property_dict_list=None, **kwargs):
        command_args = {
            'device_type': component_type,
            'device_attributes': property_dict_list,
            'component_ids': component_ids
        }

        self._lmc_dp_command_run('set_attributes', component_type, **command_args)

    @tango_error_handler
    def init_observation(self, telescope_model):
        result = self.lmc_dp.command_inout('init_observation', json.dumps(telescope_model))
        return result

    @tango_error_handler
    def start_observation(self, argin=''):
        result = self.lmc_dp.command_inout('start_observation', argin)
        return result

    @tango_error_handler
    def stop_observation(self, argin=''):
        result = self.lmc_dp.command_inout('stop_observation', argin)
        return result

    @tango_error_handler
    def get_data_file(self, station_id, tile_id, data_type):
        result = self.lmc_dp.command_inout('get_data_file', json.dumps(
            {"station_id": station_id, "tile_id": tile_id, "data_type": data_type}))
        return result

    @tango_error_handler
    def observation_data_types(self):
        result = json.loads(self.lmc_dp.command_inout('get_observation_datatypes'))
        return result

    @tango_error_handler
    def get_observation_data(self, level, type, station_id, tile_id):

        # data_type is a dictionary like:
        # {
        #     "level": "tile",
        #     "type": "burst_raw"
        # }

        # # Set logging
        # log = logging.getLogger('')
        # log.setLevel(logging.INFO)
        # line_format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        # ch = logging.FileHandler('/home/andrea/datalogger.log')
        # ch.setFormatter(line_format)
        # log.addHandler(ch)

        keys_to_get = ["daq_nof_channels",
                       "daq_nof_antennas",
                       "daq_nof_pols",
                       "daq_raw_samples",
                       "daq_channel_samples",
                       "daq_beam_samples",
                       "daq_data_directory",
                       "daq_beam_channels"]
        argin = json.dumps(keys_to_get)
        result = self.lmc_dp.command_inout("get_obsconf_params", argin)
        result = json.loads(result)

        n_channels = result["daq_nof_channels"]
        n_antennas = result["daq_nof_antennas"]
        n_pols = result["daq_nof_pols"]
        n_raw_samples = result["daq_raw_samples"]
        n_channel_samples = result["daq_channel_samples"]
        n_beam_samples = result["daq_beam_samples"]
        n_beam_channels = result["daq_beam_channels"]
        daq_data_path = result["daq_data_directory"]

        station_data_path = os.path.join(daq_data_path, str(station_id))

        data_type_info = type
        data_level_read = level

        if data_level_read == "tile":
            data_level = DataLevel.Tile
        elif data_level_read == "station":
            data_level = DataLevel.Station

        data_type_format = decode_task_type(type)

        data = []
        timestamps=[]

        data_tile_id = tile_id - 1  # LMC ids and DAQ ids have 1-based and 0-based indexes respectively
        if data_tile_id < 0:
            data_tile_id = 0

        if self.lmc_dp["is_observation_running"].value:
            if data_level is DataLevel.Tile:
                if data_type_format is JobTasks.Burst_Raw:
                    fm = RawFormatFileManager(root_path=station_data_path, daq_mode=FileDAQModes.Void)
                    fm.set_metadata(n_antennas=n_antennas, n_pols=n_pols, n_samples=n_raw_samples, n_chans=n_channels)
                    data, timestamps = fm.read_data(timestamp=None, tile_id=data_tile_id, antennas=range(0, n_antennas),
                                        polarizations=range(0, n_pols), n_samples=n_raw_samples)
                if data_type_format is JobTasks.Burst_Channel:
                    fm = ChannelFormatFileManager(root_path=station_data_path, daq_mode=FileDAQModes.Void)
                    fm.set_metadata(n_antennas=n_antennas, n_pols=n_pols, n_samples=n_channel_samples, n_chans=n_channels)
                    data, timestamps = fm.read_data(timestamp=None, tile_id=data_tile_id, antennas=range(0, n_antennas),
                                        channels=range(0, n_channels), polarizations=range(0, n_pols),
                                        n_samples=n_channel_samples)
                if data_type_format is JobTasks.Burst_Beam:
                    fm = BeamFormatFileManager(root_path=station_data_path, daq_mode=FileDAQModes.Void)
                    fm.set_metadata(n_chans=n_channels, n_pols=n_pols, n_samples=n_beam_samples)
                    data, timestamps = fm.read_data(timestamp=None, tile_id=data_tile_id, channels=range(0, n_channels),
                                        polarizations=range(0, n_pols), n_samples=n_beam_samples)
                if data_type_format is JobTasks.Integrated_Channel:
                    fm = ChannelFormatFileManager(root_path=station_data_path, daq_mode=FileDAQModes.Integrated)
                    fm.set_metadata(n_antennas=n_antennas, n_pols=n_pols,
                                    n_samples=n_channel_samples, n_chans=n_channels)
                    data, timestamps = fm.read_data(timestamp=None, tile_id=data_tile_id, antennas=range(0, n_antennas),
                                                    channels=range(0, n_channels), polarizations=range(0, n_pols),
                                                    n_samples=1)
                if data_type_format is JobTasks.Integrated_Beam:
                    fm = BeamFormatFileManager(root_path=station_data_path, daq_mode=FileDAQModes.Integrated)
                    fm.set_metadata(n_chans=n_channels, n_pols=n_pols, n_samples=n_beam_samples)
                    data, timestamps = fm.read_data(timestamp=None, tile_id=tile_id, channels=range(0, n_beam_channels),
                                                    polarizations=range(0, n_pols), n_samples=1)

            # argout = {'data': msgpack.packb(data), 'abscissa': msgpack.packb(timestamps)}
            # argout = {'data': pickle.dumps(data), 'abscissa': pickle.dumps(timestamps)}
            argout = {'data': base64.b64encode(json.dumps(data.tolist())),
                      'abscissa': base64.b64encode(json.dumps(timestamps.tolist()))}

            # data = [0, 1, 0, 0, 83, 116, -10]
            # dataStr = json.dumps(data)
            # base64EncodedStr = base64.b64encode(dataStr)
            # print(base64EncodedStr)
            # print('decoded', base64.b64decode(base64EncodedStr))

            return argout
        else:
            raise Exception("No observation is running.")

    @tango_error_handler
    def set_skymodel(self, data):
        return self.lmc_dp.command_inout('set_skymodel', json.dumps(data))

    @tango_error_handler
    def get_skymodel(self):
        return json.loads(self.lmc_dp.command_inout('get_skymodel'))

    @tango_error_handler
    def set_antenna_locations(self, data):
        self.lmc_dp.command_inout('set_antenna_locations', json.dumps(data))

    @tango_error_handler
    def get_antenna_locations(self):
        return json.loads(self.lmc_dp.command_inout('get_antenna_locations'))
