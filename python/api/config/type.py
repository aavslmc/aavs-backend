# -*- coding: utf-8 -*-

import ast

from JSONMetadataSchema import JSONMetadataSchema

TANGO_DOMAIN = 'test'
LMC_TANGO = '%s/lmc/1' % TANGO_DOMAIN
API_TANGO_MAP = {'tile': 'tile'}


def get_command_parameters(command_desc):
    non_json = ['', 'none', 'Uninitialised']
    if command_desc in non_json:
        return []
    # ugghhh POGO replaces quotes with backticks :(
    return ast.literal_eval(command_desc.replace('`', "'"))


def get_tango_address(component_type, component_id, attribute_name=None):
    params = [
        TANGO_DOMAIN,
        API_TANGO_MAP.get(component_type, component_type),
        str(component_id)]

    if attribute_name:
        params.append(attribute_name)

    return '/'.join(params)


def get_tango_device_type_id(tango_address):
    return tango_address.split('/')[1:3]


property_check_types = {
    'range': {
        'min_value': 0,
        'max_value': 0
    },
    'alarm_values':
        ['Failed', 'Incomplete', 'Timeout']
}

alarm_severity = ['Catastrophic', 'Critical', 'Marginal']
event_severity = ['Catastrophic', 'Critical', 'Marginal', 'Insignificant']

property_types = ['Metric', 'Register', 'Value']

event_types = ['Property Change', 'Command Run Event', 'State Change']

HARDWARE_COMPONENT_TYPES = ['Racks', 'Stations', 'Tiles', 'Servers', 'Pdus', 'Switches']

HARDWARE_COMPONENT_TYPES_DICT = {
    'racks': 'rack',
    'stations': 'station',
    'tiles': 'tile',
    'servers': 'server',
    'pdus': 'pdu',
    'switches': 'switch'
}


def get_singular_component_type(plural_component_type):
    return HARDWARE_COMPONENT_TYPES_DICT.get(plural_component_type, plural_component_type)


component_types = {
    'hardware': HARDWARE_COMPONENT_TYPES
}

modes = ['Control', 'Observation', 'Operating', 'Maintenance']

types = {
    'component': component_types,
    'event': event_types,
    'alarm': property_check_types,
    'observation': {'modes': {}}
}

observation_modes = {}


def add_observation_mode(observation_mode_schema, default=False):
    observation_mode = observation_mode_schema()

    observation_mode_json = JSONMetadataSchema().dump(observation_mode).data
    mode_name = observation_mode_schema.__name__.replace('ObservationModeSchema', '')
    mode_name = mode_name.lower()
    if default:
        observation_mode_json["default"] = True
    types['observation']['modes'][mode_name] = observation_mode_json
    observation_modes[mode_name] = observation_mode_schema
