from marshmallow import fields

from common import StrictSchema


class SkyModelField(StrictSchema):
    name = fields.String()
    description = fields.String()
    units = fields.String()


class SkyModelFieldList(StrictSchema):
    fields = fields.Nested(SkyModelField, many=True)


class SkyModelMetadata(StrictSchema):
    fields = fields.Nested(SkyModelField, many=True)


class SkyModel(StrictSchema):
    metadata = fields.Nested(SkyModelMetadata)
    data = fields.List(fields.List(fields.Float()))


class SkyModelSingleSchema(StrictSchema):
    skymodel = fields.Nested(SkyModel)


class SkyModelUploadSchema(StrictSchema):
    metadata = fields.Nested(SkyModelMetadata)
    data = fields.List(fields.List(fields.String()))


class SkyModelSingleUploadSchema(StrictSchema):
    skymodel = fields.Nested(SkyModelUploadSchema)


class AntennaDetailSchema(StrictSchema):
    id = fields.String()
    latitude = fields.Float()
    longitude = fields.Float()
    altitude = fields.Float()
    tile_serial = fields.String()


class AntennaLocationSchema(StrictSchema):
    antennas = fields.Nested(AntennaDetailSchema, many=True)


class AntennaLocationsSingleSchema(StrictSchema):
    telescope_model = fields.Nested(AntennaLocationSchema)
