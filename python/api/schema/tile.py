from marshmallow import fields, Schema
from common import (
    StrictSchema, HardwareSchema, CommandSchema,
    PropertySchema, FirmwareSchema, RootSchema)


class StationMemberSchema(StrictSchema):
    station_id = fields.String()
    name = fields.String()
    tile_local_name = fields.String()


class AntennaSchema(StrictSchema):
    antenna_id = fields.Integer()
    x = fields.Integer()
    y = fields.Integer()
    status = fields.String()


class TileSchema(HardwareSchema):
    stations = fields.Nested(StationMemberSchema, many=True)
    firmware = fields.Nested(FirmwareSchema)


class TileListSchema(RootSchema):
    tiles = fields.Nested(TileSchema, many=True)


class TileSingleSchema(RootSchema):
    tile = fields.Nested(TileSchema)


class MemberSchema(StrictSchema):
    component_id = fields.Integer()
    component_type = fields.String()
    name = fields.String()


class StationSchema(Schema):
    component_id = fields.Integer(dump_only=True)
    component_type = fields.String(dump_only=True)
    name = fields.String()
    commands = fields.Nested(CommandSchema, many=True)
    properties = fields.Nested(PropertySchema, many=True)
    firmware = fields.Nested(FirmwareSchema)
    members = fields.Nested(MemberSchema, many=True)


class StationListSchema(RootSchema):
    stations = fields.Nested(StationSchema, many=True)


class StationSingleSchema(RootSchema):
    station = fields.Nested(StationSchema)
