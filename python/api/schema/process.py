from marshmallow import fields
from api.schema.common import StrictSchema, get_component_type_singular, BaseComponentSchema, ParameterRunSchema


class EventBaseSchema(StrictSchema):
    timestamp = fields.DateTime(dump_only=True)
    severity = fields.String()

    event_type = fields.String()
    event_name = fields.String()

    property_name = fields.String()

    # source
    source_type = fields.Method('assert_singular_component_type')
    source_id = fields.Int()

    def assert_singular_component_type(self, context):
        component_type = context.get('source_type')
        if not component_type:
            return component_type
        return get_component_type_singular(component_type)


class EventSchema(EventBaseSchema):
    event_id = fields.UUID(dump_only=True)


class EventSingleSchema(StrictSchema):
    event = fields.Nested(EventSchema)


class EventListSchema(StrictSchema):
    events = fields.Nested(EventSchema, many=True)


class AlarmSchema(StrictSchema):
    pass


class AlarmSingleSchema(StrictSchema):
    alarm = fields.Nested(AlarmSchema)


class AlarmListSchema(StrictSchema):
    alarms = fields.Nested(AlarmSchema, many=True)


class AlarmSubscriptionSchema(StrictSchema):
    absolute = fields.String()
    min = fields.Float()
    max = fields.Float()
    component_type = fields.String()
    component_id = fields.String()
    attribute = fields.String()


class AlarmSubscriptionSingleSchema(StrictSchema):
    alarm_subscription = fields.Nested(AlarmSubscriptionSchema)


class AlarmDeleteSchema(StrictSchema):
    component_type = fields.String()
    component_id = fields.String()
    attribute = fields.String()


class AlarmDeleteSingleSchema(StrictSchema):
    alarm_subscription = fields.Nested(AlarmDeleteSchema)


class ConfiguredAlarmSchema(StrictSchema):
    name = fields.String()
    description = fields.String()
    attribute = fields.String()


class ConfiguredAlarmListSchema(StrictSchema):
    configured_alarms = fields.Nested(ConfiguredAlarmSchema, many=True)


class ActiveAlarmSchema(StrictSchema):
    name = fields.String()
    description = fields.String()
    attribute = fields.String()


class ActiveAlarmListSchema(StrictSchema):
    active_alarms = fields.Nested(ActiveAlarmSchema, many=True)


class EventSubscriptionSchema(StrictSchema):
    subscription_type = fields.String()  # Device or attribute
    component_type = fields.String(required=True)
    component_id = fields.String(required=True)
    attribute = fields.String()


class EventSubscriptionSingleSchema(StrictSchema):
    event_subscription = fields.Nested(EventSubscriptionSchema)


class EventSubscriptionListSchema(StrictSchema):
    event_subscriptions = fields.Nested(EventSubscriptionSchema, many=True)


class CommandRunResult(BaseComponentSchema):
    return_code = fields.Integer(required=True)  # 0: OK, -1: Malformed, Other: Error

    response_name = fields.String()
    response_details = fields.String()


class CommandRunSchema(BaseComponentSchema):
    command_run_id = fields.UUID()

    rack_id = fields.Integer()
    station_id = fields.Integer()
    # All commands are currently attached to a component
    component_type = fields.String(required=True)

    source = fields.String()  # Command Initiator
    command_name = fields.String(required=True)
    parameters = fields.Nested(ParameterRunSchema, many=True)
    retry_count = fields.Integer(missing=1)

    responses = fields.Nested(CommandRunResult, many=True)


class CommandRunSingleSchema(StrictSchema):
    command_run = fields.Nested(CommandRunSchema)


class CommandRunListSchema(StrictSchema):
    command_runs = fields.Nested(CommandRunSchema, many=True)


class FirmwareUploadSchema(StrictSchema):
    component_type = fields.String()
    component_id = fields.String()
    firmware_upload_id = fields.UUID()
    name = fields.String()
    version = fields.String()
    filename = fields.String()
    timestamp = fields.String()


class FirmwareUploadSingleSchema(StrictSchema):
    firmware_upload = fields.Nested(FirmwareUploadSchema)


class FirmwareUploadListSchema(StrictSchema):
    firmware_uploads = fields.Nested(FirmwareUploadSchema, many=True)
