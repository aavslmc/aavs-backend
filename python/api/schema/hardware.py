from marshmallow import fields
from common import StrictSchema, HardwareSchema, RootSchema


class RackMemberSchema(StrictSchema):
    component_id = fields.Integer()
    component_type = fields.String()
    column = fields.Integer()
    row = fields.Integer()
    width = fields.Integer()
    height = fields.Integer()


class RackSchema(HardwareSchema):
    members = fields.Nested(RackMemberSchema, many=True)


class RackListSchema(RootSchema):
    racks = fields.Nested(RackSchema, many=True)


class RackSingleSchema(RootSchema):
    rack = fields.Nested(RackSchema)