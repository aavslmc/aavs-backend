from marshmallow import Schema, fields

from api.config import HARDWARE_COMPONENT_TYPES_DICT


class StrictSchema(Schema):
    class Meta:
        strict = True


class RootSchema(StrictSchema):
    request_errors = fields.List(fields.Raw, default=[])
    request_status = fields.String(default="OK")


def get_component_type_singular(component_type):
    if component_type in HARDWARE_COMPONENT_TYPES_DICT.values():
        return component_type
    return HARDWARE_COMPONENT_TYPES_DICT.get(component_type, component_type)


class BaseComponentSchema(StrictSchema):
    component_id = fields.Integer()
    component_type = fields.Method('assert_singular_component_type')
    status = fields.String()

    def assert_singular_component_type(self, context):
        component_type = context.get('component_type')
        if component_type:
            return get_component_type_singular(component_type)


class ApiError(StrictSchema):
    error = fields.String()


class ParameterRunSchema(StrictSchema):
    name = fields.String()
    type = fields.String()
    value = fields.String(allow_none=True)


class ParameterSchema(StrictSchema):
    name = fields.String()
    type = fields.String()


class CommandSchema(BaseComponentSchema):
    name = fields.String()
    parameters = fields.Nested(ParameterSchema, many=True)


class CommandListSchema(RootSchema):
    commands = fields.Nested(CommandSchema, many=True)


class CommandSingleSchema(RootSchema):
    command = fields.Nested(CommandSchema)


class StatusSchema(BaseComponentSchema):
    value = fields.String()


class StatusSingleSchema(RootSchema):
    status = fields.Nested(StatusSchema)


class StatusListSchema(RootSchema):
    statuses = fields.Nested(StatusSchema, many=True)


class PropertySchema(BaseComponentSchema):
    name = fields.String()
    attribute_type = fields.String(dump_to="type")
    data_type = fields.String()
    polling_frequency = fields.Integer(allow_none=True)
    alarm_values = fields.List(fields.String())
    min_value = fields.Integer(allow_none=True)
    max_value = fields.Integer(allow_none=True)
    value = fields.String()  # Since no default is supplied, it will be excluded if not set
    rack_id = fields.Integer(),
    readonly = fields.Boolean()


class PropertyListSchema(RootSchema):
    properties = fields.Nested(PropertySchema, many=True)


class PropertySingleSchema(RootSchema):
    property = fields.Nested(PropertySchema)


class UpdateSchema(StrictSchema):
    updated = fields.Dict()


class MetaSchema(StrictSchema):
    meta = fields.Nested(UpdateSchema)


class FirmwareSchema(BaseComponentSchema):
    name = fields.String()
    version = fields.String()


class FirmwareSingleSchema(RootSchema):
    firmware = fields.Nested(FirmwareSchema)


class FirmwareListSchema(RootSchema):
    firmwares = fields.Nested(FirmwareSchema, many=True)


class HardwareSchema(BaseComponentSchema):
    firmware = fields.Nested(FirmwareSchema)
    commands = fields.Nested(CommandSchema, many=True)
    properties = fields.Nested(PropertySchema, many=True)


class HardwareSingleSchema(RootSchema):
    component = fields.Nested(HardwareSchema)


class HardwareListSchema(RootSchema):
    components = fields.Nested(HardwareSchema, many=True)
