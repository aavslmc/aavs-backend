from marshmallow import fields

from api.config import add_observation_mode
from api.schema.common import StrictSchema, BaseComponentSchema, CommandSchema, PropertySchema


# Observation data types schema
class ObservationSingleDataTypeSchema(StrictSchema):
    type = fields.String()
    level = fields.String()
    dimensions = fields.List(fields.String())
    unit = fields.String()


class ObservationDataTypesSchema(StrictSchema):
    types = fields.Nested(ObservationSingleDataTypeSchema, many=True)


class ObservationDataDumpSchema(StrictSchema):
    data = fields.String(required=True)
    abscissa = fields.String(required=True)


# Exposes the observation to_json
class ObservationInfoSchema(BaseComponentSchema):
    component_id = fields.String()
    commands = fields.Nested(CommandSchema, many=True)
    properties = fields.Nested(PropertySchema, many=True)


class ObservationInfoSingleSchema(BaseComponentSchema):
    observation = fields.Nested(ObservationInfoSchema)


class ObservationOperationSchema(StrictSchema):
    operation = fields.String(required=True)


class ObservationStatusSchema(StrictSchema):
    value = fields.String()


class ObservationStatusSingleSchema(StrictSchema):
    status = fields.Nested(ObservationStatusSchema)


class StationSchema(StrictSchema):
    bitfile = fields.String()
    tiles = fields.List(fields.Int())
    id = fields.Int()
    channel_integration_time = fields.Float()
    beam_integration_time = fields.Float()


class ObservationSchema(StrictSchema):
    jobs = fields.Dict()
    mode = fields.String()
    stations = fields.Nested(StationSchema, many=True)


class ObservationSingleSchema(StrictSchema):
    observation = fields.Nested(ObservationSchema)


class ObservationListSchema(StrictSchema):
    observations = fields.Nested(ObservationSchema, many=True)


class ChannelsSchema(StrictSchema):
    start = fields.Integer(description="Start Channel", default=0)
    count = fields.Integer(description="Number of channels", default=0)


class RaDecSchema(StrictSchema):
    ra = fields.Float(description="Right Ascension", units="deg", default=0)
    dec = fields.Float(description="Declination", units="deg", default=0)


class AltAzSchema(StrictSchema):
    alt = fields.Float(description="Altitude", units="deg", default=0)
    az = fields.Float(description="Azimuth", units="deg", default=0)


class MWAPointingSchema(StrictSchema):
    channels = fields.Nested(ChannelsSchema, description="Subband Selection")
    altaz = fields.Nested(AltAzSchema, description="altaz", title="altaz")
    radec = fields.Nested(RaDecSchema, description="radec", title="radec")
    atten = fields.Float(description="Attenuation", units="tbd")


class MWADaqSchema(StrictSchema):
    task_noncycle = fields.String(description="Continuously running jobs",
                                  title="task_noncycle",
                                  default="integrated_channel")
    task_cycle = fields.String(description="Cycled jobs",
                               title="task_cycle",
                               default="burst_raw,1")
    receiver_ports = fields.String(description="Receiver ports", title="receiver_ports", default="4660")
    receiver_interface = fields.String(description="Receiver interface", title="receiver_interface", default="lo")


class MWABandpassCalibrationSchema(StrictSchema):
    pass


class MWAObservationModeSchema(StrictSchema):
    daq = fields.Nested(MWADaqSchema, description="DAQ Job", title="daq")
    bandpass_calibration = fields.Nested(MWABandpassCalibrationSchema, description="Bandpass Calibration Job",
                                         title="bandpass_calibration")


class DefaultObservationModeSchema(StrictSchema):
    pointing = fields.Dict(description="Pointing Job")
    calibration = fields.Dict(description="Calibration Job")


class MockObservationModeSchema(StrictSchema):
    pointing = fields.Nested(MWAPointingSchema, description="Pointing Job")
    calibration = fields.Nested(MWAPointingSchema, description="Pointing Job")


add_observation_mode(MWAObservationModeSchema, default=True)
add_observation_mode(MockObservationModeSchema)
