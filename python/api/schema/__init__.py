from api.schema.common import (
    HardwareSchema, HardwareSingleSchema, HardwareListSchema,
    CommandSchema, CommandSingleSchema, CommandListSchema,
    PropertySchema, PropertySingleSchema, PropertyListSchema,
    FirmwareSchema, FirmwareSingleSchema, FirmwareListSchema,
    StatusSchema, StatusSingleSchema, StatusListSchema,
    ApiError,
    MetaSchema
)


from api.schema.hardware import RackSchema, RackSingleSchema, RackListSchema
from api.schema.tile import (
    TileSchema, TileSingleSchema, TileListSchema,
    StationSchema, StationSingleSchema, StationListSchema,
)
from api.schema.process import (
    AlarmSchema, AlarmSingleSchema, AlarmListSchema,
    EventSchema, EventSingleSchema, EventListSchema,
    FirmwareUploadSchema, FirmwareUploadSingleSchema, FirmwareUploadListSchema,
    CommandRunSchema, CommandRunSingleSchema, CommandRunListSchema,
    EventSubscriptionSchema, EventSubscriptionSingleSchema, EventSubscriptionListSchema,
    AlarmSubscriptionSingleSchema
)

from api.schema.observation import (
    ObservationSingleSchema, ObservationListSchema, ObservationSchema,
    ObservationOperationSchema, ObservationStatusSchema,
    MWAObservationModeSchema, DefaultObservationModeSchema, ObservationDataDumpSchema
)

# Definitions for Swagger $ref
DEFINITIONS_TO_REGISTER = {
    'Hardware': HardwareSchema,
    'Command': CommandSchema,
    'CommandRun': CommandRunSchema,
    'EventSubscription': EventSubscriptionSchema,
    'FirmwareUpload': FirmwareUploadSchema,
    'Property': PropertySchema,
    'Rack': RackSchema,
    'Tile': TileSchema,
    'Station': StationSchema,
    'Alarm': AlarmSchema,
    'Event': EventSchema,
    'Firmware': FirmwareSchema,
    'Status': StatusSchema,
    'Observation': ObservationSchema,
    'ObservationOperation': ObservationOperationSchema,
    'MWAObservationMode': MWAObservationModeSchema,
    'DefaultObservationMode': DefaultObservationModeSchema,
    'ObservationStatusSchema': ObservationStatusSchema,
    'ApiError': ApiError,
    'Meta': MetaSchema,

}
