from flask_apispec import doc

from api.config import get_singular_component_type
from api.library import ComponentManager
from api.resources.property import PropertyResource, PropertyResourceList


def properties_endpoint(app, docs, path, component_type, manager=ComponentManager, pk='component_id', pk_type='int'):
    """

    :param app:
    :param docs:
    :param path:
    :param component_type:
    :param manager:
    :param pk:
    :param pk_type:
    :return:
    """
    endpoint = component_type.lower() + '_properties'

    component_manager = manager()
    view_func = PropertyResource.as_view(
        endpoint, resource_manager=component_manager, resource_type=get_singular_component_type(component_type))

    app.add_url_rule(
        '%s/%s/<%s:%s>/properties/<string:property_name>' % (path, component_type, pk_type, pk),
        view_func=view_func,
        methods=['GET', 'PATCH'])

    doc_decorator = doc(tags=[component_type])
    docs.register(doc_decorator(PropertyResource), endpoint=endpoint)


def properties_list_endpoint(app, docs, path, component_type, manager=ComponentManager, pk='component_id',
                             pk_type='int'):
    """

    :param app:
    :param docs:
    :param path:
    :param component_type:
    :param manager:
    :param pk:
    :param pk_type:
    :return:
    """
    endpoint = '{}_propertieslist'.format(component_type.lower())
    component_manager = manager()
    view_func = PropertyResourceList.as_view(
        endpoint, resource_manager=component_manager, resource_type=get_singular_component_type(component_type))

    app.add_url_rule(
        '%s/%s/<%s:%s>/properties' % (path, component_type, pk_type, pk),
        view_func=view_func,
        methods=['GET', 'PATCH'])

    doc_decorator = doc(tags=[component_type])
    docs.register(doc_decorator(PropertyResourceList), endpoint=endpoint)
