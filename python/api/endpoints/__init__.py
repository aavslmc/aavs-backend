from admin import tango_status_endpoint, skymodel_endpoint
from alarms import alarms_endpoint
from api.library import ComponentManager
from api.schema import DEFINITIONS_TO_REGISTER
from component_types import types_endpoint
from events import event_subscriptions_endpoint
from hardware import read_update_hardware_resource_endpoint, read_update_hardware_resource_list_endpoint
from observations import observations_endpoint
from properties import properties_endpoint, properties_list_endpoint
from status import status_endpoint

component_manager = ComponentManager


def register_spec_components(docs):
    for name, schema in DEFINITIONS_TO_REGISTER.items():
        docs.spec.definition(name, schema=schema)


def register_hardware_component_endpoints(app, docs, view, list_view, component_uri_config):
    """
    Register component's endpoints

    :param app:
    :param docs:
    :param view:
    :param list_view:
    :param component_uri_config:
    :return:
    """
    # Tango Compatible components can use the ComponentTangoManager
    _component_manager = component_manager
    properties_endpoint(app, docs, manager=_component_manager, **component_uri_config)
    properties_list_endpoint(app, docs, manager=_component_manager, **component_uri_config)
    status_endpoint(app, docs, manager=_component_manager, **component_uri_config),
    read_update_hardware_resource_endpoint(app, docs, view, manager=_component_manager, **component_uri_config)
    read_update_hardware_resource_list_endpoint(app, docs, list_view, manager=_component_manager,
                                                **component_uri_config)


def register_standalone_endpoints(app, docs, path):
    """
    Register stand alone endpoints

    :param app:
    :param docs:
    :param path:
    :return:
    """
    alarms_endpoint(app, docs, path)
    event_subscriptions_endpoint(app, docs, path)
    observations_endpoint(app, docs, path, component_manager)
    skymodel_endpoint(app, docs, component_manager, path)
    tango_status_endpoint(app, docs, path)

    types_endpoint(app, docs, path, 'observation/modes', 'observation', subtype='modes')
