from flask_apispec import doc

from api.config import get_singular_component_type
from api.library import ComponentManager


def read_update_hardware_resource_endpoint(app, docs, view, path, component_type, manager=ComponentManager,
                                           pk='component_id', pk_type='int'):
    component_manager = manager()
    view_func = view.as_view(component_type, resource_manager=component_manager,
                             resource_type=get_singular_component_type(component_type))

    app.add_url_rule(
        '%s/%s/<%s:%s>' % (path, component_type, pk_type, pk),
        view_func=view_func,
        methods=['GET', 'PATCH'])
    doc_decorator = doc(tags=[component_type])
    docs.register(doc_decorator(view), endpoint=component_type)


def read_update_hardware_resource_list_endpoint(app, docs, view, path, component_type, manager=ComponentManager):
    endpoint = component_type + '_list'
    component_manager = manager()
    view_func = view.as_view(endpoint, resource_manager=component_manager,
                             resource_type=get_singular_component_type(component_type))
    app.add_url_rule(
        '%s/%s' % (path, component_type),
        view_func=view_func,
        methods=['GET'])
    doc_decorator = doc(tags=[component_type])
    docs.register(doc_decorator(view), endpoint=endpoint)
