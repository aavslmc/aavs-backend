from flask_apispec import doc

from api.config import get_singular_component_type
from api.library import ComponentManager
from api.resources.status import StatusResource


def status_endpoint(app, docs, path, component_type, manager=ComponentManager, pk='component_id', pk_type='int'):
    """

    :param app:
    :param docs:
    :param path:
    :param component_type:
    :param manager:
    :param pk:
    :param pk_type:
    :return:
    """
    endpoint = component_type.lower() + '_status'

    component_manager = manager()
    view_func = StatusResource.as_view(
        endpoint, resource_manager=component_manager, resource_type=get_singular_component_type(component_type))

    app.add_url_rule(
        '%s/%s/<%s:%s>/status' % (path, component_type, pk_type, pk),
        view_func=view_func,
        methods=['GET'])

    doc_decorator = doc(tags=[component_type])
    docs.register(doc_decorator(StatusResource), endpoint=endpoint)
