from flask_apispec import doc

from api.resources import TypeResource


def types_endpoint(app, docs, path_prefix, path_suffix, type_, subtype=None):
    """
    
    :param app:
    :param docs:
    :param path_prefix:
    :param path_suffix:
    :param type_:
    :param subtype:
    :return:
    """
    tag = 'types'

    endpoint = 'types_%s%s' % (type_, subtype or '')
    view_func = TypeResource.as_view(endpoint, type_=type_, subtype=subtype)
    path = '%s/types/%s' % (path_prefix, path_suffix)
    app.add_url_rule(
        path.lower(),
        view_func=view_func,
        methods=['GET'])

    doc_decorator = doc(tags=[tag])
    docs.register(doc_decorator(TypeResource), endpoint=endpoint)
