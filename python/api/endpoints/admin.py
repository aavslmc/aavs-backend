from flask_apispec import doc

from api.resources.admin import SkyModelUploadResource, AntennaLocationsResource, TangoStatusResource


def skymodel_endpoint(app, docs, resource_manager, path):
    endpoint = 'skymodelupload'
    view_func = SkyModelUploadResource.as_view(endpoint,
                                               resource_manager=resource_manager())

    app.add_url_rule('%s/admin/skymodel' % (path,),
                     view_func=view_func,
                     methods=['POST', 'GET'])

    doc_decorator = doc(tags=['admin'])
    docs.register(doc_decorator(SkyModelUploadResource), endpoint=endpoint)


def tango_status_endpoint(app, docs, path):
    endpoint = 'tango_status_endpoint'
    view_func = TangoStatusResource.as_view(endpoint)

    app.add_url_rule('%s/admin/tango_status' % (path,),
                     view_func=view_func,
                     methods=['GET'])

    doc_decorator = doc(tags=['admin'])
    docs.register(doc_decorator(TangoStatusResource), endpoint=endpoint)
