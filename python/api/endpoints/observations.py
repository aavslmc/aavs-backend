from flask_apispec import doc

from api.resources import ObservationResource, ObservationResourceList, ObservationDataTypesResource, \
    ObservationStatusResource, ObservationDataDumpResource


def observations_endpoint(app, docs, path, manager):
    """

    :param app:
    :param docs:
    :param path:
    :param manager:
    :return:
    """
    endpoint = 'observationslist'
    _manager = manager()
    view_func = ObservationResourceList.as_view(
        endpoint, resource_manager=_manager, resource_type="observation")

    app.add_url_rule(
        '%s/observations' % (path,),
        view_func=view_func,
        methods=['POST'])

    doc_decorator = doc(tags=['observations'])
    docs.register(doc_decorator(ObservationResourceList), endpoint=endpoint)

    endpoint = 'observations'
    view_func = ObservationResource.as_view(
        endpoint, resource_manager=_manager)
    app.add_url_rule(
        '%s/observations/current' % (path,),
        view_func=view_func,
        methods=['GET', 'PATCH', 'DELETE'])
    docs.register(doc_decorator(ObservationResource), endpoint=endpoint)

    endpoint = 'observations_status'
    view_func = ObservationStatusResource.as_view(
        endpoint, resource_manager=_manager, resource_type="observation")
    app.add_url_rule(
        '%s/observations/current/status' % (path,),
        view_func=view_func,
        methods=['GET'])
    docs.register(doc_decorator(ObservationStatusResource), endpoint=endpoint)

    endpoint = "observation_data_types"
    view_func = ObservationDataTypesResource.as_view(
        endpoint, resource_manager=_manager)
    app.add_url_rule(
        '%s/observations/current/data_types' % (path,),
        view_func=view_func,
        methods=['GET']
    )
    docs.register(doc_decorator(ObservationDataTypesResource), endpoint=endpoint)

    endpoint = "observation_data"
    view_func = ObservationDataDumpResource.as_view(
        endpoint, resource_manager=_manager)
    app.add_url_rule(
        '%s/observations/current/data' % (path,),
        view_func=view_func,
        methods=['GET']
    )
    docs.register(doc_decorator(ObservationDataDumpResource), endpoint=endpoint)
