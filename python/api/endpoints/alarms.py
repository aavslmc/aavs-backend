from flask_apispec import doc

from api.library import AlarmTangoManager
from api.resources import AlarmResourceList


def alarms_endpoint(app, docs, path):
    """

    :param app:
    :param docs:
    :param path:
    :return:
    """

    endpoint = 'alarms'
    alarm_manager = AlarmTangoManager()

    endpoint += '_list'
    view_func = AlarmResourceList.as_view(endpoint, resource_manager=alarm_manager)
    app.add_url_rule(
        '%s/alarms' % (path,),
        view_func=view_func,
        methods=['POST', 'DELETE'])
    doc_decorator = doc(tags=['alarms'])
    docs.register(doc_decorator(AlarmResourceList), endpoint=endpoint)
