from flask_apispec import doc

from api.library import EventTangoManager
from api.resources import EventSubscriptionResource, EventSubscriptionResourceList


def event_subscriptions_endpoint(app, docs, path, ):
    endpoint = 'eventsubscriptionslist'
    event_manager = EventTangoManager()
    view_func = EventSubscriptionResourceList.as_view(
        endpoint, resource_manager=event_manager)

    app.add_url_rule(
        '%s/event_subscriptions' % (path,),
        view_func=view_func,
        methods=['GET', 'POST'])

    doc_decorator = doc(tags=['event_subscriptions'])
    docs.register(doc_decorator(EventSubscriptionResourceList), endpoint=endpoint)

    endpoint = 'eventsubscriptions'
    view_func = EventSubscriptionResource.as_view(
        endpoint, resource_manager=event_manager)
    app.add_url_rule(
        '%s/event_subscriptions' % (path,),
        view_func=view_func,
        methods=['DELETE'])

    doc_decorator = doc(tags=['event_subscriptions'])
    docs.register(doc_decorator(EventSubscriptionResource), endpoint=endpoint)
