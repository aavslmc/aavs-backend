import flask.views
from apispec import APISpec
from flask_apispec.extension import FlaskApiSpec

from resources import TileResource, TileResourceList, HardwareResource, HardwareResourceList, RackResource, \
    RackResourceList

from endpoints import register_hardware_component_endpoints, register_spec_components, register_standalone_endpoints
from config.version import version, API_PREFIX

app = flask.Flask(__name__)

app.config.update({
    'APISPEC_SPEC': APISpec(
        title='AAVS LMC API',
        version=version,
        plugins=(
            'apispec.ext.marshmallow',
        ),
    ),
    'APISPEC_SWAGGER_URL': '/swagger/',
})

docs = FlaskApiSpec(app)

register_spec_components(docs)

register_standalone_endpoints(app, docs, API_PREFIX)

tiles_uri_config = {'component_type': 'tiles', 'path': API_PREFIX}
register_hardware_component_endpoints(app, docs, TileResource, TileResourceList, tiles_uri_config)

hardware_components = [
    {'config': {'component_type': 'racks', 'path': API_PREFIX},
     'resource': RackResource,
     'resource_list': RackResourceList},
    {'config': {'component_type': 'pdus', 'path': API_PREFIX},
     'resource': HardwareResource,
     'resource_list': HardwareResourceList},
    {'config': {'component_type': 'switches', 'path': API_PREFIX},
     'resource': HardwareResource,
     'resource_list': HardwareResourceList},
    {'config': {'component_type': 'servers', 'path': API_PREFIX},
     'resource': HardwareResource,
     'resource_list': HardwareResourceList},
]

for component in hardware_components:
    register_hardware_component_endpoints(app, docs,
                                          component['resource'],
                                          component['resource_list'],
                                          component['config'])
