"""
Note to self: use_kwargs(ModelSchema) should not specify location when ModelSchema contains nested models
"""

from flask_apispec import marshal_with, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import fields

from api.schema import ApiError

DEFAULT_SKIP = 0
DEFAULT_LIMIT = 0

enable_paging = use_kwargs({
    'skip': fields.Integer(missing=DEFAULT_SKIP),
    'limit': fields.Integer(missing=DEFAULT_LIMIT),
},
    locations=('query',)
)


@marshal_with(None, code=404)
@marshal_with(ApiError, code=400)
@marshal_with(ApiError, code=500)
class BaseResource(MethodResource):
    def __init__(self, resource_manager=None, resource_type=None):
        super(BaseResource, self).__init__()
        self.resource_manager = resource_manager
        self.resource_type = resource_type