from flask_apispec import marshal_with

from api.resources.common import BaseResource
from api.resources.helpers import api_error_handler
from api.schema import (
    StatusSingleSchema, StatusListSchema
)
from common import enable_paging


class StatusResource(BaseResource):
    @marshal_with(StatusSingleSchema, 200)
    @api_error_handler
    def get(self, component_id, **kwargs):
        return self.resource_manager.get_status(
            self.resource_type,
            component_id=component_id, with_value=True,
            **kwargs
        )


class StatusResourceList(BaseResource):
    @enable_paging
    @marshal_with(StatusListSchema, 200)
    @api_error_handler
    def get(self, component_id=None, skip=None, limit=None):
        component_ids = [component_id] if component_id else []
        return self.resource_manager.filter_status(
            self.resource_type,
            component_ids=component_ids,
            skip=skip, limit=limit, with_value=True
        )