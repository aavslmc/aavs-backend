import dateutil.parser
from dateutil.tz import tzutc
from flask_apispec import marshal_with, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import fields

from api.resources.helpers import api_error_handler
from api.schema import (
    EventSingleSchema, EventListSchema,
    ApiError
)
from common import enable_paging


@marshal_with(None, code=404)
@marshal_with(ApiError, code=400)
@marshal_with(ApiError, code=500)
class EventBaseResource(MethodResource):
    def __init__(self, resource_manager=None):
        super(EventBaseResource, self).__init__()
        self.resource_manager = resource_manager


class EventResource(EventBaseResource):
    @marshal_with(EventSingleSchema, code=200)
    @api_error_handler
    def get(self, event_id):
        return self.resource_manager.get(event_id)


class EventResourceList(EventBaseResource):
    @enable_paging
    @use_kwargs({
        'source_type': fields.String(),
        'source_id': fields.Integer(),
        'event_type': fields.String(),
        'event_name': fields.String(),
        'severity': fields.String(),
        'from_timestamp': fields.String()  # ms get lost when using fields.DateTime
    },
        locations=('query',)
    )
    @marshal_with(EventListSchema, code=200)
    @api_error_handler
    def get(self, source_type=None, source_id=None,
            event_type=None, event_name=None,
            severity=None, from_timestamp=None,
            skip=None, limit=None):
        if from_timestamp:
            from_timestamp = dateutil.parser.parse(from_timestamp, tzinfos=[tzutc()])
        return self.resource_manager.filter(
            source_type=source_type,
            source_id=source_id,
            event_type=event_type,
            event_name=event_name,
            from_timestamp=from_timestamp,
            severity=severity,
            skip=skip,
            limit=limit
        )
