from flask import make_response
from flask_apispec import marshal_with, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import fields

from api.resources.helpers import api_error_handler
from api.schema import (
    AlarmSubscriptionSingleSchema,
    ApiError
)
from api.schema.process import AlarmDeleteSingleSchema, ConfiguredAlarmListSchema, ActiveAlarmListSchema


@marshal_with(None, code=404)
@marshal_with(ApiError, code=400)
@marshal_with(ApiError, code=500)
class AlarmBaseResource(MethodResource):
    """
    An alarm is either an out of threshold property value or an event type greater than marginal
    """

    def __init__(self, resource_manager=None):
        super(AlarmBaseResource, self).__init__()
        self.resource_manager = resource_manager


class AlarmActiveResourceList(AlarmBaseResource):
    @use_kwargs({"component_type": fields.String(), "component_id": fields.String()}, locations=("query",))
    @marshal_with(ActiveAlarmListSchema, code=200)
    @api_error_handler
    def get(self, component_type=None, component_id=None):
        return self.resource_manager.get_active(component_type, component_id)


class AlarmResourceList(AlarmBaseResource):
    @use_kwargs({"component_type": fields.String(), "component_id": fields.String()}, locations=("query",))
    @marshal_with(ConfiguredAlarmListSchema, code=200)
    @api_error_handler
    def get(self, component_type=None, component_id=None):
        return self.resource_manager.get_configured(component_type, component_id)

    @use_kwargs(AlarmSubscriptionSingleSchema)
    @api_error_handler
    def post(self, alarm_subscription):
        self.resource_manager.add_alarm_subscription(absolute=alarm_subscription.get('absolute'),
                                                     min=alarm_subscription.get('min'),
                                                     max=alarm_subscription.get('max'),
                                                     component_type=alarm_subscription['component_type'],
                                                     component_id=alarm_subscription['component_id'],
                                                     attribute=alarm_subscription['attribute'])
        return make_response('', 201)

    @use_kwargs(AlarmDeleteSingleSchema)
    @marshal_with(None, code=204)
    @api_error_handler
    def delete(self, alarm_subscription):
        self.resource_manager.remove_alarm_subscription(component_type=alarm_subscription['component_type'],
                                                        component_id=alarm_subscription['component_id'],
                                                        attribute=alarm_subscription['attribute'])
        return make_response('', 204)
