from flask_apispec import marshal_with, use_kwargs, doc
from marshmallow import fields

from api.resources.helpers import api_error_handler
from api.schema import (
    StationSingleSchema, StationListSchema,
    TileSingleSchema, TileListSchema
)
from common import enable_paging, BaseResource


@doc(
    params={'component_id': {'description': 'The station id'}},
)
class StationResource(BaseResource):
    @marshal_with(StationSingleSchema, code=200)
    @marshal_with(None, code=404)
    @api_error_handler
    def get(self, component_id):
        return self.resource_manager.get(
            self.resource_type,
            component_id=component_id)


class StationResourceList(BaseResource):
    @enable_paging
    @use_kwargs({'status': fields.String()})
    @marshal_with(StationListSchema, code=200)
    @marshal_with(None, code=404)
    @api_error_handler
    def get(self, status=None, skip=None, limit=None):
        return self.resource_manager.filter(
            self.resource_type,
            status=status,
            skip=skip,
            limit=limit)


@doc(
    params={'component_id': {'description': 'The tile id'}},
)
class TileResource(BaseResource):
    @marshal_with(TileSingleSchema, code=200)
    @marshal_with(None, code=404)
    @api_error_handler
    def get(self, component_id):
        return self.resource_manager.get(
            self.resource_type,
            component_id=component_id)


@doc(
    params={'station_id': {'description': 'Filter tiles by station_id'}}
)
class TileResourceList(BaseResource):
    @enable_paging
    @use_kwargs({'station_id': fields.Integer()}, locations=('query',))
    @marshal_with(TileListSchema, code=200)
    @api_error_handler
    def get(self, station_id=None, skip=None, limit=None):
        station_ids = [station_id] if station_id else None
        return self.resource_manager.filter(
            self.resource_type,
            station_ids=station_ids,
            skip=skip, limit=limit)
