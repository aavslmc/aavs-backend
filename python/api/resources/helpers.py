from flask import make_response
from api.library import ResourceNotFoundException
from api.library.common import BadRequestException


def api_error_handler(function):
    """
    Common error handling functionality
    :param function: request handler function
    :return:
    """
    def wrapper(self, *args, **kwargs):
        try:
            if not self.resource_manager:
                return make_response('', 404)
            return function(self, *args, **kwargs)
        except ResourceNotFoundException as ex:
            return make_response(str(ex.message), 404)
        except BadRequestException as ex:
            return {'error': ex}, 400
        except Exception, ex:

            return {'error': ex}, 500
    return wrapper
