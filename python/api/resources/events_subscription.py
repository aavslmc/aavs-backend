from flask import make_response
from flask_apispec import marshal_with, use_kwargs, doc
from flask_apispec.views import MethodResource
from marshmallow import fields

from api.resources.helpers import api_error_handler
from api.schema import (
    EventSubscriptionSingleSchema, EventSubscriptionListSchema,
    ApiError
)


@marshal_with(None, code=404)
@marshal_with(ApiError, code=400)
@marshal_with(ApiError, code=500)
class EventSubscriptionBaseResource(MethodResource):
    """
    An alarm is either an out of threshold property value or an event type greater than marginal
    """

    def __init__(self, resource_manager=None):
        super(EventSubscriptionBaseResource, self).__init__()
        self.resource_manager = resource_manager


class EventSubscriptionResource(EventSubscriptionBaseResource):
    @use_kwargs(EventSubscriptionSingleSchema)
    @marshal_with(None, code=204)
    @api_error_handler
    def delete(self, event_subscription, ):
        self.resource_manager.delete(event_type=event_subscription['subscription_type'],
                                     component_type=event_subscription['component_type'],
                                     component_id=event_subscription['component_id'],
                                     attribute=event_subscription['attribute'])
        return make_response('', 204)


class EventSubscriptionResourceList(EventSubscriptionBaseResource):
    @use_kwargs({
        'event_type': fields.String(),
        'component_type': fields.String(),
        'component_id': fields.Integer(),
        'attribute': fields.String()
    },
        locations=('query',)
    )
    @marshal_with(EventSubscriptionListSchema)
    @api_error_handler
    def get(self, event_type=None, component_type=None,
            component_id=None,
            attribute=None,
            ):
        return self.resource_manager.filter(
            event_type=event_type,
            component_type=component_type,
            component_id=component_id,
            attribute=attribute)

    @use_kwargs(EventSubscriptionSingleSchema)
    @marshal_with(EventSubscriptionSingleSchema, 200)
    @api_error_handler
    @doc(
        description="""
            subscription_type: 'device' or 'attribute'
            device_name: eg 'tile'
            device_id: eg 1
            attribute_name: name of attribute, only used if type is attribute

            A subscription can be on a device, for events pushed by the device code,
            or on a attribute, which pushes events when a change occurs on the attribute.
            """
    )
    def post(self, event_subscription=None, **kwargs):
        """

        :param event_subscription:
        :param kwargs:
        :return:
        """
        self.resource_manager.add(event_type=event_subscription['subscription_type'],
                                  component_type=event_subscription['component_type'],
                                  component_id=event_subscription['component_id'],
                                  attribute=event_subscription['attribute'])

        response = make_response('', 201)
        return response


