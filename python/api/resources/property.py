from flask_apispec import marshal_with, use_kwargs
from marshmallow import fields

from api.resources.common import BaseResource
from api.resources.helpers import api_error_handler
from api.schema import (
    PropertySingleSchema, PropertyListSchema,
    MetaSchema
)
from common import enable_paging


class PropertyResource(BaseResource):
    @use_kwargs({'with_value': fields.Boolean(missing=False)}, locations=('query',))
    @marshal_with(PropertySingleSchema, 200)
    @api_error_handler
    def get(self, property_name, component_id=None, with_value=False, **kwargs):
        data = self.resource_manager.get_property(
            self.resource_type,
            component_id=component_id,
            property_name=property_name,
            with_value=with_value,
            **kwargs
        )
        return data

    @use_kwargs(PropertySingleSchema)
    @marshal_with(MetaSchema, 200)
    @api_error_handler
    def patch(self, property_name, property, component_id=None):
        component_ids = [component_id] if component_id else []
        return self.resource_manager.update_properties(
            self.resource_type,
            property_name=property_name,
            component_ids=component_ids,
            property=property
        )


class PropertyResourceList(BaseResource):
    @enable_paging
    @use_kwargs({'with_value': fields.Boolean(missing=False)}, locations=('query',))
    @marshal_with(PropertyListSchema, 200)
    @api_error_handler
    def get(self, component_id=None, property_name=None, with_value=None, skip=None, limit=None):
        component_ids = [component_id] if component_id else []
        return self.resource_manager.filter_properties(
            self.resource_type,
            component_ids=component_ids,
            property_name=property_name,
            with_value=with_value,
            skip=skip, limit=limit
        )

    @use_kwargs(PropertyListSchema)
    @marshal_with(MetaSchema, 200)
    @api_error_handler
    def patch(self, properties, component_id=None, **kwargs):
        component_ids = [component_id] if component_id else []
        return self.resource_manager.update_properties(
            self.resource_type,
            component_ids=component_ids,
            properties=properties
        )
