import subprocess

from flask import make_response
from flask_apispec import marshal_with, use_kwargs, doc

from api.resources.common import BaseResource
from api.resources.helpers import api_error_handler
from api.schema.admin import SkyModelSingleSchema, SkyModelSingleUploadSchema, \
    AntennaLocationsSingleSchema


class SkyModelUploadResource(BaseResource):
    # NB The order of the decorators matters!
    # the webargs and swagger decorators MUST be before any others
    @doc(description="multipart/form with 1 json object and 1 file")
    @use_kwargs(SkyModelSingleUploadSchema)
    @api_error_handler
    def post(self, skymodel=None, **kwargs):
        data = skymodel["data"]
        field_data = skymodel["metadata"]["fields"]

        self.resource_manager.set_skymodel({"skymodel": {
            "metadata": {
                "fields": field_data
            },
            "data": data}})
        return make_response('', 200)

    @marshal_with(SkyModelSingleSchema, 200)
    @api_error_handler
    def get(self, **kwargs):
        return self.resource_manager.get_skymodel()


class AntennaLocationsResource(BaseResource):
    @use_kwargs(AntennaLocationsSingleSchema)
    @api_error_handler
    def post(self, telescope_model):
        self.resource_manager.set_antenna_locations({"telescope_model": telescope_model})
        return make_response('', 200)

    @api_error_handler
    def get(self):
        return self.resource_manager.get_antenna_locations()


class TangoStatusResource(BaseResource):
    def get(self):
        status_output = subprocess.check_output(["aavsctl", "--status", "--use_json"])
        return make_response(status_output, 200)
