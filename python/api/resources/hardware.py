from flask_apispec import marshal_with

from api.resources.helpers import api_error_handler
from api.schema import (
    HardwareSingleSchema, HardwareListSchema,
    RackSingleSchema, RackListSchema
)
from common import enable_paging, BaseResource


class HardwareResource(BaseResource):
    """
    Generic View to Cater for single instances of pdus, switches and servers
    """

    @marshal_with(HardwareSingleSchema, code=200)
    @api_error_handler
    def get(self, component_id):
        return self.resource_manager.get(self.resource_type, component_id=component_id)


class HardwareResourceList(BaseResource):
    """
    Generic View to Cater for lists of pdus, switches and servers
    """

    @enable_paging
    @marshal_with(HardwareListSchema, code=200)
    @api_error_handler
    def get(self, skip=None, limit=None):

        return self.resource_manager.filter(self.resource_type, skip=skip, limit=limit)


class RackResource(BaseResource):

    @marshal_with(RackSingleSchema, code=200)
    @api_error_handler
    def get(self, component_id):
        return self.resource_manager.get(self.resource_type, component_id=component_id)


class RackResourceList(BaseResource):

    @enable_paging
    @marshal_with(RackListSchema, code=200)
    @api_error_handler
    def get(self, status=None, skip=None, limit=None):

        return self.resource_manager.filter(
                self.resource_type,
                status=None,
                skip=skip, limit=limit)
