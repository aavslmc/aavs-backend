import json

from flask import make_response
from flask import send_file
from flask.ext.apispec.views import MethodResource
from flask_apispec import marshal_with, use_kwargs, doc
from marshmallow import fields

from api.config import observation_modes
from api.config.version import API_PREFIX
from api.library.common import ObservationExistsException
from api.resources.helpers import api_error_handler
from api.schema import ApiError
from api.schema import (
    ObservationSingleSchema, ObservationOperationSchema
)
from api.schema.observation import ObservationStatusSingleSchema, ObservationInfoSingleSchema, ObservationDataTypesSchema, ObservationDataDumpSchema
from common import BaseResource


# TODO implement api methods
class ObservationResource(BaseResource):
    @marshal_with(ObservationInfoSingleSchema)
    def get(self):
        return self.resource_manager.get(component_type="observation", component_id="obs", with_value=True)

    @doc(description='Operations available: "start | stop".')
    @use_kwargs(ObservationOperationSchema)
    @api_error_handler
    def patch(self, operation=None):
        if operation not in ["start", "stop"]:
            raise Exception("Available operations are: start | stop")

        if operation == "start":
            ret = self.resource_manager.start_observation()
        elif operation == "stop":
            ret = self.resource_manager.stop_observation()
        if not ret:
            ret = ""
        response = make_response(ret, 202)
        uri = '%s/observations/current/status' % (API_PREFIX,)
        response.headers['Location'] = uri
        return response

    @marshal_with(None, code=204)
    @api_error_handler
    def delete(self, **kwargs):
        ret = self.resource_manager.stop_observation()
        ret = "OK" if ret else "Not OK"
        return make_response(ret, 204)


class ObservationResourceList(BaseResource):
    @use_kwargs({'status': fields.String()}, locations=('query',))
    # TODO: do we want to to return just the TM or do we want to marshal with schema?
    @api_error_handler
    def get(self, status=None):
        return self.resource_manager.filter(
            self.resource_type)

    @use_kwargs(ObservationSingleSchema)
    @api_error_handler
    def post(self, observation=None, **kwargs):
        if not observation:
            return {'error': 'No observation specified in post'}, 400
        mode = observation.get('mode')
        if mode not in observation_modes:
            raise Exception(
                'Mode %s not supported, mode must be one of %s' % (
                    mode, ', '.join(observation_modes.keys())))

        # TODO: maybe just validate?
        try:
            ret = self.resource_manager.init_observation(observation)
        except ObservationExistsException as ex:
            response = make_response(json.dumps({"error": "Observation {} already exists ".format(ex.message)}), 409)
            uri = '%s/observations/%s' % (API_PREFIX, ex.message)
            response.headers['Location'] = uri
            return response

        response = make_response('', 201)
        uri = '%s/observations/%s' % (API_PREFIX, json.loads(ret)["observation_id"]
                                      )
        response.headers['Location'] = uri
        response.data = ret
        return response


class ObservationStatusResource(BaseResource):
    @marshal_with(ObservationStatusSingleSchema)
    @api_error_handler
    def get(self):
        return self.resource_manager.get_status(
            self.resource_type,
            component_id="obs", with_value=True)


class ObservationDataTypesResource(BaseResource):
    @marshal_with(ObservationDataTypesSchema)
    @marshal_with(None, code=404)
    @api_error_handler
    def get(self):
        return self.resource_manager.observation_data_types()


@marshal_with(None, code=404)
class ObservationDataDumpResource(BaseResource):
    @use_kwargs({"level": fields.String(), "type": fields.String(), "station_id": fields.Int(), "tile_id": fields.Int()}, locations=('query',))
    @marshal_with(ObservationDataDumpSchema)
    @api_error_handler
    def get(self, level, type, station_id=None, tile_id=None):
        result = self.resource_manager.get_observation_data(level, type, station_id, tile_id)
        return result


