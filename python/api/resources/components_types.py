from flask import make_response
from flask_apispec import marshal_with, use_kwargs
from flask_apispec.views import MethodResource

from marshmallow import fields

from api.config import HARDWARE_COMPONENT_TYPES_LOWER
from api.config.type import types, get_singular_component_type

from api.schema import ApiError
from api.resources.common import BaseResource
from api.resources.helpers import api_error_handler


class TypeComponentCommonResource(BaseResource):

    @use_kwargs({'rack_id': fields.Integer(missing=None)}, locations=('query',))
    @marshal_with(None, 404)
    @api_error_handler
    def get(self, component_type, component_attribute, rack_id=None):
        if not component_type.lower() in HARDWARE_COMPONENT_TYPES_LOWER:
            return make_response('', 404)
        component_type = get_singular_component_type(component_type)

        rack_ids = [rack_id] if rack_id else []
        if component_attribute.lower() == 'properties':
            return self.resource_manager.common_properties(component_type, rack_ids)

        if component_attribute.lower() == 'commands':
            return self.resource_manager.common_commands(component_type, rack_ids)

        return make_response('', 404)


@marshal_with(ApiError, code=400)
@marshal_with(ApiError, code=500)
class TypeResource(MethodResource):

    def __init__(self, type_=None, subtype=None):
        self.type_ = type_
        self.subtype = subtype

    @marshal_with(None, 404)
    def get(self):
        if self.type_ not in types:
            return make_response('', 404)
        type_data = types[self.type_]

        if self.type_ == 'event':
            if not self.subtype in HARDWARE_COMPONENT_TYPES_LOWER:
                return make_response('', 404)
            # All components have the same events for now
            return {'%s_%s' % (self.type_, self.subtype): type_data}

        if not self.subtype:
            return {self.type_: type_data}, 200

        if self.subtype not in type_data:
            return make_response('', 404)

        return {self.subtype: type_data[self.subtype]}, 200
