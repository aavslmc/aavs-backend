import logging as log
import random
from datetime import datetime
from time import sleep

alarms = [
    {
        "type": "ALARM",  # INFO | ALARM | ALARM_OFF
        "timestamp": "2016-05-12 11:41:59.475104",
        "ID": "test/observation/obs/state_alarm",
        "reasons": ["Alarm triggered on test/observation/obs/state=ALARM"],
        "attribute": "test/observation/obs/state",
    },
    {
        "type": "NORMAL",  # INFO | ALARM | ALARM_OFF
        "timestamp": "2016-05-12 11:41:59.475104",
        "ID": "test/observation/obs/state_alarm",
        "reasons": ["Alarm triggered on test/observation/obs/state=ALARM"],
        "attribute": "test/observation/obs/state",
    },
    {
        "type": "ALARM",  # INFO | ALARM | ALARM_OFF
        "timestamp": "2016-05-12 12:41:59.475104",
        "ID": "test/tile/1/members_state_alarm",
        "reasons": ["Alarm triggered on test/tile/1/members_state=FAULT"],
        "attribute": "test/tile/1/members_state",
    }
]

events = [
    {"type": "device",  # INFO | ALARM | ALARM_OFF
     "timestamp": "2016-05-12 11:41:59.475104",
     "source": "test/Observation/1",
     "description": "Observation started",
     "device_state": "ON"
     },
    {"type": "attribute",  # INFO | ALARM | ALARM_OFF
     "timestamp": "2016-05-12 11:41:59.475104",
     "source": "test/tile/1/lmc_port",
     "description": "Attribute lmc_port on test/tile/1 changed to 1234",
     "device_state": ""
     }]


def create_metric(device_name, device_id, attr_name, base, metric_range):
    """
    Create a mock metric for a device
    
    :param device_name: 
    :param device_id: 
    :param attr_name: 
    :param base: 
    :param metric_range:
    :return: 
    """
    dev = 100 / metric_range
    return {
        "component_id": str(device_id),
        "component_type": device_name,
        "name": "test/{}/{}/{}".format(device_name, device_id, attr_name),
        "value": base + (random.random() * 100) / dev,
        "timestamp": str(datetime.now())
    }


def mock_metrics():
    """
    Yield mock metrics for tiles and server
    :return: 
    """

    # Create tile metrics
    for r in range(1, 11):
        yield create_metric("tile", r, "temperature", 60, 25)
        yield create_metric("tile", r, "voltage", 30, 25)

    # Create server metrics
    for s in range(1, 3):
        yield create_metric("server", s, "temperature", 60, 25)
        yield create_metric("server", s, "voltage", 30, 25)


def mock_metric_emitter(sio, domain):
    """
    Publish mock metric data to the clients
    :param sio: The Socket.IO instance
    :param domain: The domain on which the events are emitted

    :return:
    """
    log.info("Background thread started")

    while True:
        for metric_data in mock_metrics():
            sio.emit('metric', {"data": metric_data}, namespace='/metrics')
            log.debug('{}/metric: {}'.format(domain, metric_data))
        sleep(1)


def mock_event_emitter(sio, domain):
    """
    Publish mock event data to the clients
    :param sio: The Socket.IO instance
    :param domain: The domain on which the events are emitted

    :return:
    """

    log.info("Background thread started")

    while True:
        for event_data in events:
            sio.emit('event', {"data": event_data}, namespace='/events')
            log.debug('{}/event: {}'.format(domain, event_data))
        sleep(1)


def mock_alarm_emitter(sio, domain):
    """
    Publish mock alarm data to the clients
    :param sio: The Socket.IO instance
    :param domain: The domain on which the events are emitted
    :return:
    """
    log.info("Background thread started")

    while True:
        for alarm_data in alarms:
            sio.emit('alarm', {"data": alarm_data}, namespace='/alarms')
            log.debug('{}/alarm: {}'.format(domain, alarm_data))
            sleep(2)
