import json
import logging as log
from datetime import datetime
from time import sleep

import PyTango
from gevent import Timeout

LIMIT = 50


def _iter_emitter(stream_proxy, get_data):
    """
    Iterate over the items in a stream proxy
    :param stream_proxy:
    :param get_data:
    :return:
    """

    while True:
        start = datetime.now()
        for data in get_data(stream_proxy):
            yield data

        sleep_time = 1 - (datetime.now() - start).total_seconds()

        if sleep_time < 0:
            sleep_time = 0.5

        sleep(sleep_time)


def _pop_item(stream_proxy):
    """
    Pop the item from the stream proxy

    :param stream_proxy:
    :return:
    """
    try:
        items = json.loads(stream_proxy.command_inout("popx", LIMIT, timeout=0.5))
        for item in items:
            try:
                yield item
            except Exception as e:
                log.error(e)
    except Timeout as t:
        log.debug(str(t))
    except PyTango.DevFailed as df:
        log.debug(str(df))


def _emit(sio, domain, stream_device_name, namespace, channel):
    """
    Emit the data from a stream device on to a channel on a namespace

    :param sio: The Socket.IO socket
    :param domain: The domain of the Socket.IO socket
    :param stream_device_name: The name of the device from which a stream proxy will be created
    :param namespace: The namespace of the channel
    :param channel: The channel on which to publish
    :return:
    """
    stream_proxy = PyTango.DeviceProxy(stream_device_name, green_mode=PyTango.GreenMode.Gevent)
    for data in _iter_emitter(stream_proxy, _pop_item):
        sio.emit(channel, {"data": data}, namespace='/' + namespace)
        log.debug('{}/alarm: {}'.format(domain, data))


def alarm_emitter(sio, domain):
    """
    Publish alarm data to the clients

    :param sio: The Socket.IO socket
    :param domain:
    :return:
    """

    # The stream device for alarms
    stream_device_name = domain + "/alarmstream/1"

    # Emit the stream data to the clients
    _emit(sio, domain, stream_device_name, 'alarms', 'alarm')


def event_emitter(sio, domain):
    """
    Publish alarm data to the clients

    :param sio: The Socket.IO socket
    :param domain:
    :return:
    """

    # The stream device for events
    stream_device_name = domain + "/eventstream/1"

    # Emit the stream data to the clients
    _emit(sio, domain, stream_device_name, 'events', 'event')
