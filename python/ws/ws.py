import logging as log
import logging.config as log_config
import os
import sys

from flask import Flask
from flask_socketio import SocketIO
from gevent import monkey, spawn

from api.config.version import TANGO_DOMAIN
from metrics import metric_emitter
from mock import mock_metric_emitter, mock_event_emitter, mock_alarm_emitter
from stream import alarm_emitter, event_emitter

monkey.patch_all()

config_filepath = os.path.join(os.path.dirname(__file__), '../configuration.ini')
log_config.fileConfig(config_filepath, disable_existing_loggers=False)

if 'ws.ws:prod' in sys.argv:
    log.info('Getting metrics, alarms and events from TANGO ({})'.format(TANGO_DOMAIN))
    prod = Flask(__name__)
    prod.config['SECRET_KEY'] = 'cb2291345a49a550a006cd9a071a523ab4f44d277c9ebd3c'
    sio = SocketIO(prod, port=8123)

    spawn(metric_emitter, sio, TANGO_DOMAIN)
    spawn(event_emitter, sio, TANGO_DOMAIN)
    spawn(alarm_emitter, sio, TANGO_DOMAIN)
elif 'ws.ws:mock' in sys.argv:
    log.info('Getting MOCK metrics, alarms and events')
    mock = Flask(__name__)
    mock.config['SECRET_KEY'] = 'cb2291345a49a550a006cd9a071a523ab4f44d277c9ebd3c'
    sio = SocketIO(mock, port=8123)

    spawn(mock_metric_emitter, sio, "test")
    spawn(mock_event_emitter, sio, "test")
    spawn(mock_alarm_emitter, sio, "test")
else:
    raise ValueError("APP_MODULE is not valid")


@sio.on('connect')
def connect():
    log.info('connected')


@sio.on('subscribe')
def subscribe():
    log.info('subscribed')


@sio.on('connect', namespace='/events')
def connect_events():
    log.info('[events] connected')
    return True


@sio.on('connect', namespace='/alarms')
def connect():
    log.info('[alarms] connected')
    return True


@sio.on('connect', namespace='/metric')
def connect():
    log.info('[metric] connected')
    return True
