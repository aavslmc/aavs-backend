import logging as log
from collections import namedtuple
from datetime import datetime
from time import sleep

import PyTango


def get_all_metrics(domain):
    """
    Get all the metric data from Tango

    :param domain:
    :return: The PyTango response
    """
    g = PyTango.Group("")
    g.add(domain + "/tile/*")
    g.add(domain + "/server/*")
    g.add(domain + "/pdu/*")
    g.add(domain + "/switch/*")

    metrics = dict()
    group_reply = g.command_inout("get_metrics")

    Attribute = namedtuple("Attribute", ["proxy", "domain", "type", "id", "attribute"])
    for reply in group_reply:
        if not reply.has_failed():
            device_type, device_id = reply.dev_name().split("/")[1:3]

            # Create proxy to device to check whether it is in FAULT state
            dp = PyTango.DeviceProxy(reply.dev_name())
            if dp['state'].value == PyTango.DevState.FAULT:
                continue

            metric_names = reply.get_data()
            for metric_name in metric_names:
                if metric_name == '':
                    continue
                attr_name = reply.dev_name() + "/" + metric_name
                try:
                    metrics[attr_name] = Attribute(PyTango.AttributeProxy(attr_name), domain, device_type, device_id,
                                                   attr_name)
                except Exception as e:
                    log.error(e.message)
        else:
            log.warn("Reply failed: {} {}".format(reply.dev_name(), reply.get_err_stack()))
    return metrics


def metric_data(metrics):
    """
    Yield the metrics data from PyTango's response
    :param metrics: PyTango's metrics response
    :return:
    """
    timestamp = datetime.now()
    for name, attribute in metrics.iteritems():
        try:
            value = attribute.proxy.read().value
            yield {
                "component_id": attribute.id,
                "component_type": attribute.type,
                "name": attribute.attribute,
                "value": value,
                "timestamp": str(timestamp)
            }
        except Exception as e:
            log.error(e)


def metric_emitter(sio, domain):
    """
    Publish metric data to the clients
    :param sio: The Socket.IO instance
    :param domain: The domain on which the events are emitted

    :return:
    """

    count = 0
    while True:
        metrics = get_all_metrics(domain)
        for m in metric_data(metrics):
            sio.emit('metric', {"data": m}, namespace='/metrics')
            log.debug('{}/metric: {}'.format(domain, m))
        count += 1
        sleep(2)
