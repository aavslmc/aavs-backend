#!/usr/bin/env bash
# Deploy script for AAVS API
# Assumption: env vars are set by caller
# $AAVS_PYTHON - should be virtualenv directory - not bin folder of venv containing python executable
# $AAVS_PATH  - root folder of install
# $AAVS_LOG - log location
# (do not run with sudo)
echo -e "\n==== Configuring AAVS Backend ====\n"

# Installing required system packages
echo "Installing required system packages"
sudo apt-get -q install --force-yes --yes $(grep -vE "^\s*#" requirements.apt  | tr "\n" " ")

# Installing required python packages
pip install -r requirements.pip

export setupdir=$AAVS_PATH/aavs-backend/setup
export aavs_sock=/tmp/aavs.sock
echo $setupdir
echo aavs_sock

# Nginx setup
mkdir -p $AAVS_LOG/nginx
sudo chown $USER:$USER $AAVS_LOG/nginx

sudo -E bash -c "sed \
    -e \"s:%%log%%:$AAVS_LOG:\" \
    -e \"s:%%socket%%:$aavs_sock:\" \
$setupdir/config/nginx/aavs-api.template.conf > /etc/nginx/sites-available/aavs-api.conf"

if [ ! -L /etc/nginx/sites-enabled/aavs-api.conf ]; then
  echo "Creating nginx symlink"
  sudo ln -s /etc/nginx/sites-available/aavs-api.conf /etc/nginx/sites-enabled/aavs-api.conf
else
 echo "Confirmed nginx symlink exists"
fi
# remove default site if exists, as it will shadow our own site
sudo rm /etc/nginx/sites-available/default

sudo -E cp $setupdir/config/nginx/uwsgi_params /etc/nginx
sudo service nginx restart

# uwsgi setup
mkdir -p $AAVS_LOG/uwsgi
sudo chown $USER:$USER $AAVS_LOG/uwsgi

sed \
    -e "s:%%user%%:$USER:" \
    -e "s:%%pyenv%%:$AAVS_PYTHON:" \
    -e "s:%%pwd%%:$AAVS_PATH/aavs-backend/python:" \
    -e "s:%%log%%:$AAVS_LOG:" \
    -e "s:%%socket%%:$aavs_sock:" \
$setupdir/config/uwsgi/aavs-api.template.ini > $setupdir/config/uwsgi/aavs-api.ini

sudo -E mv $setupdir/config/uwsgi/aavs-api.ini /etc/uwsgi/apps-available/aavs-api.ini

if [ ! -L /etc/uwsgi/apps-enabled/aavs-api.ini ]; then
  echo "Creating uwsgi symlink"
  sudo ln -s /etc/uwsgi/apps-available/aavs-api.ini /etc/uwsgi/apps-enabled/aavs-api.ini
else
 echo "Confirmed uwsgi symlink exists"
fi

sed -e "s:%%path%%:$AAVS_PATH/aavs-backend/python:" \
$setupdir/config/init/aavs-ws.template.conf > $setupdir/config/aavs-ws.conf

sudo -E mv $setupdir/config/aavs-ws.conf /etc/init/

# upstart setup
echo "Creating upstart script and starting service"
sudo cp $setupdir/config/init/aavs-api.conf /etc/init/
sudo initctl reload-configuration
sudo service aavs-api restart
sudo service aavs-ws restart

