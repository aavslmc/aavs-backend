# AAVS LMC Backend

## Getting Started

### Installation

The installation instructions and deployment details for the AAVS LMC can be found [here](https://bitbucket.org/aavslmc/aavs-system/src/14f6742c60010a68e01c78312c30e18aaa961b79/?at=bootstrap). The AAVS LMC Backend should be installed through the `boostrap.sh` script available at the aavs-system bootstrap branch.

The status of the aavs can be checked through, 

```bash
aavsctl --status
```

Once the installation of the AAVS LMC is complete, the TPM simulator can be installed,

```bash
cd AAVS/
git clone git@bitbucket.org:aavslmc/tpm-simulator.git
./deploy.sh
```

## Run the API

Activate the AAVS environment

```bash
aavs_env
```

Run `aavs_ctl`

```bash
aavsctl --stop
aavsctl --run
```

Run the API server

```bash
python AAVS/aavs-backend/python/runserver.py
```

## Running Gunicorn 

**Note:** Gunicorn was chosen as  `uwsgi` is problematic with websockets

Activate the AAVS environment

```bash
aavs_env
```

Run the Gunicorn server in production mode

```bash
cd AAVS/aavs-backend/python
/opt/aavs/python/bin/gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 ws.ws:prod -b 127.0.0.1:8123
```

Run the Gunicorn server (mock data)

```bash
cd AAVS/aavs-backend/python
/opt/aavs/python/bin/gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 ws.ws:mock -b 127.0.0.1:8123
```

Running Client

```bash
cd AAVS/aavs-backend/python/ws/tests
python client.py
```

The Gunicorn process can be killed through
```bash
sudo kill -9 `ps aux |grep gunicorn  | awk '{ print $2 }'` 
```


## Running the Unit Tests
```bash
cd AAVS/
python -m unittest discover -s AAVS/aavs-backend/python/tests/api -t AAVS/aavs-backend/python
```

### Websockets testing
Tests on websocket is not done through unit tests but through an end-to-end tests as a client, that can be run through,
```bash
cd AAVS/aavs-backend/python/tests/ws
python test_client.py --events --alarms --metrics
```

If the gunicorn process (mock or not) is running, the client should display events, alarms and metrics

## Usage

The API is available through

- [http://localhost:5000/aavs/v2](http://localhost:5000/aavs/v2 "API")

The API specification can be accessed through

- [http://localhost:5000/swagger-ui/](http://localhost:5000/swagger-ui/ "Swagger UI")

The API specification JSON (`python/swagger.json`) is made available at

- [http://localhost:5000/swagger/](http://localhost:5000/swagger/ "Swagger JSON Spec")


### Troubleshooting

**When**: Trying to access a particular api endpoint

**Error**: `Failed to execute command_inout_asynch on device test/server/1, command to_json"`

**Fix**: Restart the aavsctl utility with `aavsctl --run`



**When**: Trying to run the dev server

**Error**: `device test/lmc/1 not defined in the database`

**Fix**: Restart the aavsctl utility with `aavsctl --run`


## Authors
- Stephanie Buhagiar
- Bernard Brincat
- Denis Cutajar
- Dr Andrea DeMarco
- Dr Alessio Magro
